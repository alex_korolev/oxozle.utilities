﻿// *********************************************************************************
// Author:	Azarov Dmitriy
// Email:	oxozle@gmail.com
// Date: 	30.06.2014
// Project:	Oxozle.Utilities.Web.OxoDirectoryHelper.cs
//  
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// *********************************************************************************

#region Usings

using System.IO;
using System.Linq;

#endregion

namespace Oxozle.Utilities.Web
{
    public enum FileExtensions
    {
        All = 0,
        XLS = 1,
        CSV = 2,
        DOCX = 3
    }

    public static class OxoDirectoryHelper
    {
        private static readonly string[] BlockedExtensions =
        {
            ".config",
            ".exe",
            ".aspx",
            ".asp",
            ".cs",
            ".asax",
            ".chtml"
        };

        /// <summary>
        /// Файлы с определенным расширением
        /// </summary>
        public static string[] GetFilesWithExtension(string folder, FileExtensions extension)
        {
            switch (extension)
            {
                case FileExtensions.XLS:
                    return Directory.GetFiles(folder, "*.xls");
                case FileExtensions.CSV:
                    return Directory.GetFiles(folder, "*.csv");
            }

            return Directory.GetFiles(folder);
        }

        /// <summary>
        /// Вовзращает True, если файл заблокирован по расширению
        /// </summary>
        public static bool IsBlockedExtension(string file)
        {
            if (BlockedExtensions.Contains(Path.GetExtension(file)))
                return true;

            return false;
        }
    }
}