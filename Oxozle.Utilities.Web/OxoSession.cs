﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Oxozle.Utilities.Web
{
    /// <summary>
    ///   Работа с сессией
    /// </summary>
    public static class OxoSession
    {
        /// <summary>
        ///   Возвращает элемент из сессии
        /// </summary>
        public static object GetValue(string key)
        {
            if ((HttpContext.Current != null) && (HttpContext.Current.Session != null))
            {
                return HttpContext.Current.Session[key];
            }
            return null;
        }

        /// <summary>
        ///   Удаляет элемент из сессии
        /// </summary>
        /// <param name="key"> </param>
        public static void Remove(string key)
        {
            if (((HttpContext.Current != null) && (HttpContext.Current.Session != null)) &&
                (HttpContext.Current.Session[key] != null))
            {
                HttpContext.Current.Session.Remove(key);
            }
        }

        /// <summary>
        ///   Устанавливает элемент в сессию
        /// </summary>
        public static void SetValue(string key, object value)
        {
            if ((HttpContext.Current != null) && (HttpContext.Current.Session != null))
            {
                HttpContext.Current.Session[key] = value;
            }
        }


        /// <summary>
        ///   Возвращает True если в сесии есть элемент с ключем key
        /// </summary>
        /// <param name="key"> </param>
        /// <returns> </returns>
        public static bool Contains(string key)
        {
            if (HttpContext.Current != null && HttpContext.Current.Session != null)
                return HttpContext.Current.Session[key] != null;

            return false;
        }
    }
}
