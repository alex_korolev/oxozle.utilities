﻿// *********************************************************************************
// Author:	Azarov Dmitriy
// Email:	oxozle@gmail.com
// Date: 	30.06.2014
// Project:	Oxozle.Utilities.Web.OxoCookies.cs
//  
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// *********************************************************************************

#region Usings

using System;
using System.Collections;
using System.Web;

#endregion

namespace Oxozle.Utilities.Web
{
    /// <summary>
    /// Обертка для работы с Cookies
    /// </summary>
    public static class OxoCookies
    {
        public static Hashtable RequestCookies
        {
            get
            {
                Hashtable item = (Hashtable)OxoHttpContextRequestStock.GetItem("RequestCookies");
                if (((item == null) && (HttpContext.Current != null)) && (HttpContext.Current.Request != null))
                {
                    item = new Hashtable();
                    if (HttpContext.Current.Request.Cookies != null)
                    {
                        foreach (string str in HttpContext.Current.Request.Cookies.AllKeys)
                        {
                            item[str] = HttpContext.Current.Request.Cookies[str].Value;
                        }
                    }
                    OxoHttpContextRequestStock.Add("RequestCookies", item);
                }
                return item;
            }
        }

        public static Hashtable ResponseCookies
        {
            get
            {
                Hashtable item = (Hashtable)OxoHttpContextRequestStock.GetItem("ResponseCookies");
                if (((item == null) && (HttpContext.Current != null)) && (HttpContext.Current.Response != null))
                {
                    item = new Hashtable();
                    if (HttpContext.Current.Response.Cookies != null)
                    {
                        foreach (string str in HttpContext.Current.Response.Cookies.AllKeys)
                        {
                            item[str] = HttpContext.Current.Response.Cookies[str].Value;
                        }
                    }
                    OxoHttpContextRequestStock.Add("ResponseCookies", item);
                }
                return item;
            }
        }

        /// <summary>
        /// True когда возможна работа с Cookies
        /// </summary>
        private static bool IsValidCookies()
        {
            return (HttpContext.Current != null && HttpContext.Current.Response != null &&
                    HttpContext.Current.Response.Cookies != null);
        }

        public static void SetValue(string cookieName, string cookieObject, DateTime dateStoreTo, string domain = null)
        {
            if (!IsValidCookies())
                return;

            HttpCookie cookie = HttpContext.Current.Response.Cookies[cookieName];
            if (cookie == null)
            {
                cookie = new HttpCookie(cookieName);
                cookie.Path = "/";
            }

            cookie.Value = cookieObject;
            cookie.Expires = dateStoreTo;

            if (!domain.IsEmpty() && !domain.Equals(".localhost"))
                cookie.Domain = domain;

            HttpContext.Current.Response.SetCookie(cookie);
            OxoHttpContextRequestStock.Remove("ResponseCookies");
        }


        public static void Remove(string name)
        {
            if (!IsValidCookies())
                return;

            SetValue(name, "", DateTime.Now.AddYears(-1));
            OxoHttpContextRequestStock.Remove("ResponseCookies");
        }
    }
}