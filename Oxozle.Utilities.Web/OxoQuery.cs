﻿// *********************************************************************************
// Author:	Azarov Dmitriy
// Email:	oxozle@gmail.com
// Date: 	30.06.2014
// Project:	Oxozle.Utilities.Web.OxoQuery.cs
//  
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// *********************************************************************************

#region Usings

using System;
using System.Collections.Specialized;
using System.Web;

#endregion

namespace Oxozle.Utilities.Web
{
    public static class OxoQuery
    {
        /// <summary>
        ///   Часть адреса в запросе для указания страницы
        /// </summary>
        public const string PageQuery = "page";

        /// <summary>
        ///   Часть адреса в запросе для указания кол-во объектов на странице
        /// </summary>
        public const string PageSizeQuery = "psize";

        public static NameValueCollection QueryString
        {
            get
            {
                if (HttpContext.Current == null || HttpContext.Current.Request == null)
                {
                    return null;
                }

                return HttpContext.Current.Request.QueryString;
            }
        }

        public static string[] QueryStringKeys
        {
            get
            {
                if (QueryString != null)
                    return QueryString.AllKeys;

                return new string[0];
            }
        }

        /// <summary>
        ///   Возвращает Raw URL
        /// </summary>
        public static string RawUrl
        {
            get
            {
                if (HttpContext.Current != null && HttpContext.Current.Request != null)
                    return HttpContext.Current.Request.RawUrl;

                return null;
            }
        }

        /// <summary>
        ///   Возвращает Request URL
        /// </summary>
        public static Uri RequestUrl
        {
            get
            {
                if (HttpContext.Current != null && HttpContext.Current.Request != null)
                    return HttpContext.Current.Request.Url;

                return null;
            }
        }

        /// <summary>
        ///   Возвращает Request URL
        /// </summary>
        public static string UrlReferrer
        {
            get
            {
                if (HttpContext.Current != null && HttpContext.Current.Request != null)
                    if (HttpContext.Current.Request.UrlReferrer != null)
                        return HttpContext.Current.Request.UrlReferrer.ToString();

                return null;
            }
        }


        /// <summary>
        /// Возвращает Raw URL
        /// </summary>
        public static string OriginalRequestString
        {
            get
            {
                if (HttpContext.Current != null && HttpContext.Current.Request != null)
                    return HttpContext.Current.Request.Url.OriginalString;

                return null;
            }
        }

        /// <summary>
        ///   Возвращает URL хоста
        /// </summary>
        public static string Host
        {
            get
            {
                if (HttpContext.Current != null && HttpContext.Current.Request != null)
                    return HttpContext.Current.Request.Url.Host;
                return null;
            }
        }

        public static string SecondLevelDomain
        {
            get
            {
                if (HttpContext.Current != null && HttpContext.Current.Request != null)
                {
                    var splitHostName = Host.Split('.');
                    if (splitHostName.Length >= 2)
                    {
                        var secondLevelHostName = splitHostName[splitHostName.Length - 2] + "." +
                                                  splitHostName[splitHostName.Length - 1];
                        return secondLevelHostName;
                    }
                }
                if (OxoRequest.IsLocalHost)
                    return "localhost";

                return null;
            }
        }


        /// <summary>
        ///   Номер страницы из запроса
        /// </summary>
        public static int GetPageValue()
        {
            return GetInteger(PageQuery, 1);
        }

        public static bool Contains(string name)
        {
            if (HttpContext.Current == null)
            {
                return false;
            }
            return (HttpContext.Current.Request.QueryString[name] != null);
        }

        public static bool GetBoolean(string name, bool defaultValue)
        {
            if (HttpContext.Current == null)
            {
                return defaultValue;
            }
            return OxoValidation.GetBoolean(HttpContext.Current.Request.QueryString[name], defaultValue);
        }

        public static double GetDouble(string name, double defaultValue)
        {
            if (HttpContext.Current == null)
            {
                return defaultValue;
            }
            try
            {
                return OxoValidation.GetDouble(HttpContext.Current.Request.QueryString[name], defaultValue);
            }
            catch
            {
                return defaultValue;
            }
        }

        public static int GetInteger(string name, int defaultValue)
        {
            if (HttpContext.Current != null)
            {
                string str = HttpContext.Current.Request.QueryString[name];
                return str.GetInteger(defaultValue);
            }
            return defaultValue;
        }


        /// <summary>
        ///   Адрес для разбиения на страницы
        /// </summary>
        /// <returns> </returns>
        public static string GetBasePagedUrl()
        {
            return GetBasePagedUrlWork();
        }


        /// <summary>
        ///   Адрес для разбиения на страницы
        /// </summary>
        /// <returns> </returns>
        public static string GetBasePagedUrlTemplate()
        {
            return GetBasePagedUrlWork() + "{0}";
        }


        private static string GetBasePagedUrlWork()
        {
            QueryStringBuilder builder = new QueryStringBuilder();

            foreach (string key in QueryString)
            {
                builder.Add(key, QueryString[key]);
            }

            builder.Remove("page");
            builder.Add("page", string.Empty);

            return RequestUrl.AbsolutePath + builder.ToString();

        }


        public static string GetString(string name, string defaultValue)
        {
            if (HttpContext.Current == null)
            {
                return defaultValue;
            }
            return OxoValidation.GetString(HttpContext.Current.Request.QueryString[name], defaultValue);
        }
    }
}