﻿
using System.Web.Mvc;
using Oxozle.Utilities.Pages;

namespace Oxozle.Utilities.Web.Pages
{
    /// <summary>
    ///   Класс для данных поддерживающие разбиение на страницы
    /// </summary>
    public class PagedView<T>
    {
        private PagedList<T> _data;
        private Pagination _pagination;
        private string _pages;
        private string _showed;

        public string BaseUrl { get; set; }

        public PagedView(PagedList<T> pagedList)
            : this(pagedList, PaginationType.Public)
        {
        }

        public PagedView(PagedList<T> pagedList, PaginationType type)
            : this(pagedList, type, 3)
        {
        }

        public PagedView(PagedList<T> pagedList, PaginationType type, int numLink)
            : this(pagedList, type, numLink, null)
        {

        }



        public PagedView(PagedList<T> pagedList, PaginationType type, int numLink, string anchore)
        {
            _data = pagedList;
            _pagination = new Pagination(pagedList);
            _pagination.AnchorName = anchore;
            _pagination.BaseUrl = OxoQuery.GetBasePagedUrl();
            _pagination.NumLinks = numLink;


            _pages = _pagination.GetPageLinks();

            if (type == PaginationType.Public)
                _showed = _pagination.GetShowedInfo(PaginationShowedType.Public);
            else
                _showed = _pagination.GetShowedInfo(PaginationShowedType.Admin);
        }



        public PagedList<T> Data
        {
            get { return _data; }
        }


        public string Pages
        {
            get { return _pages; }
        }

        public MvcHtmlString Pages2
        {
            get { return Pagination2.PageLinks(Data.CurrentPage, Data.TotalPages, OxoQuery.GetBasePagedUrlTemplate()); }
        }


        public MvcHtmlString HomePagesWithPrevAndNext
        {
            get
            {
                return new MvcHtmlString(new Pagination(Data)
                {
                    BaseUrl = "/page"
                }.GetPageLinks());
                ;
            }
        }


        public MvcHtmlString TagPagesWithPrevAndNext
        {
            get
            {
                return new MvcHtmlString(new Pagination(Data)
                {
                    BaseUrl = BaseUrl + "/"
                }.GetPageLinks());

            }
        }


        /// <summary>
        ///   Показано Х - ХХ из ХХХ
        /// </summary>
        public string Showed
        {
            get { return _showed; }
        }

        /// <summary>
        /// Ссылка изменить кол-во на странице
        /// </summary>
        public string ChangePageSizeLink { get; set; }
    }
}