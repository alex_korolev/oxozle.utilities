﻿#region Usings

using System;
using System.Text;
using Oxozle.Utilities.Pages;

#endregion

namespace Oxozle.Utilities.Web.Pages
{
    public enum PaginationShowedType
    {
        Admin,

        Public
    }

    /// <summary>
    ///   Pagination type enumeration
    /// </summary>
    public enum PaginationType
    {

        /// <summary>
        ///   Обновленная версия
        /// </summary>
        Public = 20,
    }



    /// <summary>
    ///   Pagination class
    /// </summary>
    public class Pagination
    {
        #region Private Properties

        #region General Properties

        private int _perPage = 10; // Max number of items you want shown per page
        private int _numLinks = 2; // Number of "digit" links to show before/after the currently viewed page

        #endregion

        #region Link Properties

        private string _firstLink = "&larr; Первая";

        private string _nextLink = "следующая";
        private string _prevLink = "предыдущая";
        private string _ctrlL = ""; //"&larr; Ctrl&nbsp;";
        private string _ctrlR = ""; //"&nbsp;Ctrl &rarr;";

        private string _lastLink = "Последняя &rarr;";

        #endregion

        #region Tag Properties

        private string _fullTagOpen = "";
        private string _fullTagClose = "";
        private string _firstTagOpen = "";
        private string _firstTagClose = "&nbsp;";
        private string _lastTagOpen = "&nbsp;";
        private string _lastTagClose = "";
        private string _curTagOpen = "&nbsp;<span class=\"current\">";
        private string _curTagClose = "</span>";
        private string _nextTagOpen = "&nbsp;";
        private string _nextTagClose = "&nbsp;";
        private string _prevTagOpen = "&nbsp;";
        private string _prevTagClose = "";
        private string _numTagOpen = "&nbsp;";
        private string _numTagClose = "";
        private string _itemTagOpen = "<span>&nbsp;&nbsp;";
        private string _itemTagClose = "</span>&nbsp;&nbsp;";

        #endregion

        //private ItemTypes _itemType = ItemTypes.Item;

        #endregion

        #region Public Property Accessors

        #region General Property Accessors

        /// <summary>
        ///   Gets or sets url to controller/action.
        /// </summary>
        public string BaseUrl { get; set; }

        /// <summary>
        ///   Gets or sets the total rows in the result set.
        /// </summary>
        public int TotalRows { get; set; }

        /// <summary>
        ///   Gets or sets the number item per page.
        /// </summary>
        public int PerPage
        {
            get { return _perPage; }
            set { _perPage = value; }
        }

        /// <summary>
        ///   Gets or sets the number of "digit" links before and after the selected page number.
        /// </summary>
        public int NumLinks
        {
            get { return _numLinks; }
            set { _numLinks = value; }
        }

        /// <summary>
        ///   Gets or sets the current selected page.
        /// </summary>
        public int CurPage { get; set; }

        #endregion

        #region Link Property Accessors

        /// <summary>
        ///   Gets or sets the text to be shown in the "first" link on the left.
        /// </summary>
        public string FirstLink
        {
            get { return _firstLink; }
            set { _firstLink = value; }
        }

        /// <summary>
        ///   Gets or sets the text to be shown in the "next" page link.
        /// </summary>
        public string NextLink
        {
            get { return _nextLink; }
            set { _nextLink = value; }
        }

        /// <summary>
        ///   Gets or sets the text to be shown in the "previous" page link.
        /// </summary>
        public string PrevLink
        {
            get { return _prevLink; }
            set { _prevLink = value; }
        }

        /// <summary>
        ///   Gets or sets the text to be shown in the "last" link on the right.
        /// </summary>
        public string LastLink
        {
            get { return _lastLink; }
            set { _lastLink = value; }
        }

        #endregion

        #region Tag Property Accessors

        /// <summary>
        ///   Gets or sets the opening tag placed on the left side of the entire result.
        /// </summary>
        public string FullTagOpen
        {
            get { return _fullTagOpen; }
            set { _fullTagOpen = value; }
        }

        /// <summary>
        ///   Gets or sets the closing tag placed on the right side of the entire result.
        /// </summary>
        public string FullTagClose
        {
            get { return _fullTagClose; }
            set { _fullTagClose = value; }
        }

        /// <summary>
        ///   Gets or sets the opening tag for the "first" link.
        /// </summary>
        public string FirstTagOpen
        {
            get { return _firstTagOpen; }
            set { _firstTagOpen = value; }
        }

        /// <summary>
        ///   Gets or sets the closing tag for the "first" link.
        /// </summary>
        public string FirstTagClose
        {
            get { return _firstTagClose; }
            set { _firstTagClose = value; }
        }

        /// <summary>
        ///   Gets or sets the opening tag for the "last" link.
        /// </summary>
        public string LastTagOpen
        {
            get { return _lastTagOpen; }
            set { _lastTagOpen = value; }
        }

        /// <summary>
        ///   Gets or sets the closing tag for the "last" link.
        /// </summary>
        public string LastTagClose
        {
            get { return _lastTagClose; }
            set { _lastTagClose = value; }
        }

        /// <summary>
        ///   Gets or sets the opening tag for the "current" link.
        /// </summary>
        public string CurTagOpen
        {
            get { return _curTagOpen; }
            set { _curTagOpen = value; }
        }

        /// <summary>
        ///   Gets or sets the closing tag for the "current" link.
        /// </summary>
        public string CurTagClose
        {
            get { return _curTagClose; }
            set { _curTagClose = value; }
        }

        /// <summary>
        ///   Gets or sets the opening tag for the "next" link.
        /// </summary>
        public string NextTagOpen
        {
            get { return _nextTagOpen; }
            set { _nextTagOpen = value; }
        }

        /// <summary>
        ///   Gets or sets the closing tag for the "next" link.
        /// </summary>
        public string NextTagClose
        {
            get { return _nextTagClose; }
            set { _nextTagClose = value; }
        }

        /// <summary>
        ///   Gets or sets the opening tag for the "previous" link.
        /// </summary>
        public string PrevTagOpen
        {
            get { return _prevTagOpen; }
            set { _prevTagOpen = value; }
        }

        /// <summary>
        ///   Gets or sets the closing tag for the "previous" link.
        /// </summary>
        public string PrevTagClose
        {
            get { return _prevTagClose; }
            set { _prevTagClose = value; }
        }

        /// <summary>
        ///   Gets or sets the opening tag for the "digit" link.
        /// </summary>
        public string NumTagOpen
        {
            get { return _numTagOpen; }
            set { _numTagOpen = value; }
        }

        /// <summary>
        ///   Gets or sets the closing tag for the "digit" link.
        /// </summary>
        public string NumTagClose
        {
            get { return _numTagClose; }
            set { _numTagClose = value; }
        }

        /// <summary>
        ///   Gets or sets the opening tag for the "Item".
        /// </summary>
        public string ItemTagOpen
        {
            get { return _itemTagOpen; }
            set { _itemTagOpen = value; }
        }

        /// <summary>
        ///   Gets or sets the closing tag for the "Item".
        /// </summary>
        public string ItemTagClose
        {
            get { return _itemTagClose; }
            set { _itemTagClose = value; }
        }

        #endregion

        ///// <summary>
        /////   Gets or sets the item type.
        ///// </summary>
        //public ItemTypes ItemType
        //{
        //    get { return _itemType; }
        //    set { _itemType = value; }
        //}

        #endregion

        /// <summary>
        ///   Gets or sets the total number of pages
        /// </summary>
        private int NumPages { get; set; }

        /// <summary>
        /// Имя якоря, в ссылках
        /// </summary>
        public string AnchorName { get; set; }

        #region Public Constructors

        public Pagination(IPagedList pagedList)
        {
            TotalRows = pagedList.TotalCount;
            CurPage = pagedList.CurrentPage;
            PerPage = pagedList.PageSize;
        }

        #endregion

        #region Public Methods

        public string GetShowedInfo(PaginationShowedType type)
        {
            // If anything invalid
            if (!this.IsValidSettings())
                return "";

            // And here we go...
            StringBuilder pageLink = new StringBuilder();

            switch (type)
            {
                case PaginationShowedType.Public:
                    this.TDRBuildShowedInfo(pageLink);
                    break;
            }

            pageLink.Insert(0, this.FullTagOpen);
            pageLink.Append(this.FullTagClose);

            return pageLink.ToString();
        }


        /// <summary>
        ///   Get the page links
        /// </summary>
        /// <param name = "pageType">Pagination type</param>
        /// <returns>Page link string</returns>
        public string GetPageLinks()
        {
            // If anything invalid
            if (!this.IsValidSettings())
                return "";

            // And here we go...
            StringBuilder pageLink = new StringBuilder();
            this.IterflixBuildPages(pageLink);
            pageLink.Insert(0, this.FullTagOpen);
            pageLink.Append(this.FullTagClose);

            return pageLink.ToString();
        }



        #endregion

        #region Private Methods

        /// <summary>
        ///   Set the Pagination Configuration Settings
        /// </summary>
        /// <param name = "propertyName">Configuration Property Name</param>
        /// <param name = "propertyValue">Configuration Property Value</param>
        private void SetPaginationSettings(string propertyName, string propertyValue)
        {
            switch (propertyName)
            {
                #region Set General Properties

                case "BaseUrl":
                    this.BaseUrl = propertyValue;
                    break;

                case "TotalRows":
                    this.TotalRows = Convert.ToInt32(propertyValue);
                    break;

                case "PerPage":
                    this.PerPage = Convert.ToInt32(propertyValue);
                    break;

                case "NumLinks":
                    this.NumLinks = Convert.ToInt32(propertyValue);
                    break;

                case "CurPage":
                    this.CurPage = Convert.ToInt32(propertyValue);
                    break;

                #endregion

                #region Set Link Properties

                case "FirstLink":
                    this.FirstLink = propertyValue;
                    break;

                case "NextLink":
                    this.NextLink = propertyValue;
                    break;

                case "PrevLink":
                    this.PrevLink = propertyValue;
                    break;

                case "LastLink":
                    this.LastLink = propertyValue;
                    break;

                #endregion

                #region Set Tag Properties

                case "FullTagOpen":
                    this.FullTagOpen = propertyValue;
                    break;

                case "FullTagClose":
                    this.FullTagClose = propertyValue;
                    break;

                case "FirstTagOpen":
                    this.FirstTagOpen = propertyValue;
                    break;

                case "FirstTagClose":
                    this.FirstTagClose = propertyValue;
                    break;

                case "LastTagOpen":
                    this.LastTagOpen = propertyValue;
                    break;

                case "LastTagClose":
                    this.LastTagClose = propertyValue;
                    break;

                case "CurTagOpen":
                    this.CurTagOpen = propertyValue;
                    break;

                case "CurTagClose":
                    this.CurTagClose = propertyValue;
                    break;

                case "NextTagOpen":
                    this.NextTagOpen = propertyValue;
                    break;

                case "NextTagClose":
                    this.NextTagClose = propertyValue;
                    break;

                case "PrevTagOpen":
                    this.PrevTagOpen = propertyValue;
                    break;

                case "PrevTagClose":
                    this.PrevTagClose = propertyValue;
                    break;

                case "NumTagOpen":
                    this.NumTagOpen = propertyValue;
                    break;

                case "NumTagClose":
                    this.NumTagClose = propertyValue;
                    break;

                #endregion

                default:
                    break;
            }
        }

        /// <summary>
        ///   Check if everythings are ok
        /// </summary>
        /// <returns>Boolean</returns>
        private bool IsValidSettings()
        {
            // If our item count or per page total is zero there is no need to continue.
            if (this.TotalRows == 0 && this.PerPage == 0)
                return false;

            // Calculate the total number of pages
            this.NumPages =
                Convert.ToInt32(Math.Ceiling(Convert.ToDouble(this.TotalRows) / Convert.ToDouble(this.PerPage)));

            // Is there only one page? Hm... nothing more to do here then.
            //if (NumPages == 1)
            //return false;

            // Is the current page equals to 0 then return
            if (this.CurPage == 0)
                return false;

            // Is the current page beyond the result range then return
            if (((this.CurPage - 1) * this.PerPage) > this.TotalRows)
                return false;

            return true;
        }

        /// <summary>
        /// Iterflix build page links
        /// </summary>
        private void IterflixBuildPages(StringBuilder pageLink)
        {
            string anchore = string.Empty;
            if (!string.IsNullOrEmpty(AnchorName))
            {
                anchore = "#" + AnchorName;
            }

            if (this.CurPage >= 2)
            {
                int i = this.CurPage - 1;

                if (i == 0)
                    i = 1;

                pageLink.Append("<a class='products-pagination-prev' href='" + this.BaseUrl + Convert.ToString(i) +
                                anchore + "'>");
                pageLink.Append("&larr; Предыдущая</a>");
            }
            else
            {
                //pageLink.Append("<span class='disabled'>&larr; Предыдущая</span>");
            }

            //pageLink.Append("&nbsp;|&nbsp;Страница:&nbsp;");
            pageLink.Append("&nbsp;");

            // Calculate the start and end numbers. These determine
            // which number to start and end the digit links with
            int start = ((this.CurPage - this.NumLinks) > 0) ? this.CurPage - (this.NumLinks - 1) : 1;
            int end = ((this.CurPage + this.NumLinks) < NumPages) ? this.CurPage + this.NumLinks : NumPages;


            if (CurPage - NumLinks >= 2)
            {
                pageLink.Append("<a href='" + this.BaseUrl + Convert.ToString(1) + anchore + "'>");
                pageLink.Append(1);
                pageLink.Append("</a>");
                if (CurPage - NumLinks != 2)
                {
                    pageLink.Append("...");
                }
            }

            for (int loop = start - 1; loop <= end; loop++)
            {
                int i = (loop * this.PerPage) - this.PerPage;

                if (i >= 0)
                {
                    // Current page
                    if (this.CurPage == loop)
                    {
                        pageLink.Append(this.CurTagOpen);
                        pageLink.Append(loop);
                        pageLink.Append(this.CurTagClose);
                    }
                    else
                    {
                        pageLink.Append(this.NumTagOpen);
                        pageLink.Append("<a href='" + this.BaseUrl + Convert.ToString(loop) + anchore + "'>");
                        pageLink.Append(loop);
                        pageLink.Append("</a>");
                        pageLink.Append(this.NumTagClose);
                    }
                }
            }

            if (CurPage + NumLinks < NumPages)
            {
                if (CurPage + NumLinks != NumPages - 1)
                    pageLink.Append("...");
                pageLink.Append("<a href='" + this.BaseUrl + Convert.ToString(NumPages) + anchore + "'>");
                pageLink.Append(NumPages);
                pageLink.Append("</a>");
            }

            pageLink.Append("&nbsp;");

            if (this.CurPage < NumPages)
            {
                pageLink.Append("<a class='products-pagination-next' href='" + this.BaseUrl +
                                Convert.ToString(this.CurPage + 1) + anchore + "'>");
                pageLink.Append("Cледующая &rarr;</a>");
            }
            else
            {
                //pageLink.Append("<span class='disabled'>Cледующая &rarr;</span>");
            }
        }


        private void TDRBuildShowedInfo(StringBuilder pageLink)
        {
            pageLink.Append(this.ItemTagOpen);

            int start = (this.CurPage - 1) * this.PerPage + 1;
            int end = this.CurPage * this.PerPage;

            end = (end > this.TotalRows) ? this.TotalRows : end;

            if (TotalRows == 0)
                start = 0;

            pageLink.Append("Показано " + start.ToString() +
                            " - " + end.ToString() +
                            " из " + this.TotalRows.ToString() + " ");


            pageLink.Append(this.ItemTagClose);
        }


        #endregion
    }
}