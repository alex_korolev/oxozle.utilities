﻿// *********************************************************************************
// Author:	Azarov Dmitriy
// Email:	oxozle@gmail.com
// Date: 	30.06.2014
// Project:	Oxozle.Utilities.Web.OxoHttpContextRequestStock.cs
//  
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// *********************************************************************************

#region Usings

using System.Collections;
using System.Web;

#endregion

namespace Oxozle.Utilities.Web
{
    /// <summary>
    /// Объекты в HttpContext.Current.Items
    /// </summary>
    public static class OxoHttpContextRequestStock
    {
        /// <summary>
        ///   Получает текущие значения
        /// </summary>
        public static IDictionary CurrentItems
        {
            get
            {
                if (HttpContext.Current != null)
                {
                    return HttpContext.Current.Items;
                }
                return null;
            }
        }

        /// <summary>
        ///   Добавить элемент
        /// </summary>
        /// <param name="key"> Ключ </param>
        /// <param name="value"> Значение </param>
        public static void Add(string key, object value)
        {
            OxoDictionaryHelper.Add(CurrentItems, key, value);
        }

        /// <summary>
        ///   Проверяет содержание элемента с ключом
        /// </summary>
        /// <param name="key"> Ключ </param>
        /// <returns> True если элемент существует </returns>
        public static bool Contains(string key)
        {
            return OxoDictionaryHelper.Contains(CurrentItems, key);
        }


        /// <summary>
        ///   Возвращает элемент с заданным ключом
        /// </summary>
        /// <param name="key"> Ключ </param>
        /// <returns> Элемент </returns>
        public static object GetItem(string key)
        {
            return OxoDictionaryHelper.GetItem(CurrentItems, key);
        }


        /// <summary>
        ///   Удаляет элемент с заданным ключом
        /// </summary>
        /// <param name="key"> Ключ </param>
        public static void Remove(string key)
        {
            OxoDictionaryHelper.Remove(CurrentItems, key);
        }
    }
}