﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Oxozle.Utilities.Web.WeblogPing.Configuration;

namespace Oxozle.Utilities.Web.WeblogPing
{

    public static class WeblogUpdateService
    {

        public static void SendPings(string title, string blogUrl)
        {
            Task.Factory.StartNew(() => ExecuteAll(title, blogUrl));
        }

        private static void ExecuteAll(string title, string blogUrl)
        {
            foreach (WeblogElement weblogElement in Configuration.WeblogConfiguration.Settings.Pings)
            {
                Execute(weblogElement.Url, title, blogUrl);
            }
        }

        /// <summary>
        /// Creates a web request and with the RPC-XML code in the stream.
        /// </summary>
        private static void Execute(string pingServiceUrl, string title, string blogUrl)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(pingServiceUrl);
                request.Method = "POST";
                request.ContentType = "text/xml";
                request.Timeout = 3000;

                AddXmlToRequest(request, title, blogUrl);
                WebResponse response = request.GetResponse() as HttpWebResponse;
                // Get the stream containing content returned by the server.
                Stream dataStream = response.GetResponseStream();
                // Open the stream using a StreamReader for easy access.
                StreamReader reader = new StreamReader(dataStream);
                // Read the content.
                string responseFromServer = reader.ReadToEnd();

                Debug.WriteLine(responseFromServer);
            }
            catch (Exception)
            {
                // Log the error.
            }
        }


        /// <summary>
        /// Adds the XML to web request. The XML is the standard
        /// XML used by RPC-XML requests.
        /// </summary>
        private static void AddXmlToRequest(HttpWebRequest request, string title, string absoluteUrl)
        {
            Stream stream = (Stream)request.GetRequestStream();
            using (XmlTextWriter writer = new XmlTextWriter(stream, Encoding.ASCII))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("methodCall");
                writer.WriteElementString("methodName", "weblogUpdates.ping");
                writer.WriteStartElement("params");
                writer.WriteStartElement("param");
                writer.WriteElementString("value", title);
                writer.WriteEndElement();
                writer.WriteStartElement("param");
                writer.WriteElementString("value", absoluteUrl);
                writer.WriteEndElement();
                writer.WriteEndElement();
                writer.WriteEndElement();
            }
        }
    }
}
