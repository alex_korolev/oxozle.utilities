﻿using System.Configuration;

namespace Oxozle.Utilities.Web.WeblogPing.Configuration
{
    public class WeblogCollection : ConfigurationElementCollection
    {
        public override ConfigurationElementCollectionType CollectionType
        {
            get
            {
                return ConfigurationElementCollectionType.AddRemoveClearMap;
            }
        }


        protected override ConfigurationElement CreateNewElement()
        {
            return new WeblogElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((WeblogElement)element).Title;
        }

        public WeblogElement this[int index]
        {
            get { return (WeblogElement)BaseGet(index); }
            set
            {
                if (BaseGet(index) != null)
                    BaseRemoveAt(index);
                BaseAdd(index, value);
            }
        }

        public void Add(WeblogElement element)
        {
            BaseAdd(element);
        }

        public void Clear()
        {
            BaseClear();
        }
        public void Remove(WeblogElement element)
        {
            BaseRemove(element.Title);
        }

        public void Remove(string name)
        {
            BaseRemove(name);
        }

        public void RemoveAt(int index)
        {
            BaseRemoveAt(index);
        }
    }

    public class WeblogElement : ConfigurationElement
    {
        [ConfigurationProperty("Title", IsRequired = true, DefaultValue = "Untitled")]
        public string Title
        {
            get { return (string)this["Title"]; }
            set { this["Title"] = value; }
        }

        [ConfigurationProperty("Url", IsRequired = false, DefaultValue = "http://ping.blogs.yandex.ru/RPC2")]
        public string Url
        {
            get { return (string)this["Url"]; }
            set { this["Url"] = value; }
        }


    }
}
