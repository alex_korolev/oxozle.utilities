﻿// *********************************************************************************
// Author:	Azarov Dmitriy
// Email:	oxozle@gmail.com
// Date: 	30.06.2014
// Project:	Oxozle.Utilities.Web.OxoRequest.cs
//  
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// *********************************************************************************

#region Usings

using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

#endregion

namespace Oxozle.Utilities.Web
{
    /// <summary>
    ///   Работа с URL
    /// </summary>
    public static class OxoRequest
    {
        /// <summary>
        ///   Возвращает Raw URL
        /// </summary>
        public static string RawUrl
        {
            get
            {
                if (HttpContext.Current != null && HttpContext.Current.Request != null)
                    return HttpContext.Current.Request.RawUrl;

                return null;
            }
        }

        /// <summary>
        ///   Возвращает Raw URL
        /// </summary>
        public static Uri RequestUrl
        {
            get
            {
                if (HttpContext.Current != null && HttpContext.Current.Request != null)
                    return HttpContext.Current.Request.Url;

                return null;
            }
        }

        /// <summary>
        ///   Возвращает URL хоста
        /// </summary>
        public static string Host
        {
            get
            {
                if (HttpContext.Current != null && HttpContext.Current.Request != null)
                    return HttpContext.Current.Request.Url.Host;
                return null;
            }
        }


        /// <summary>
        ///   Возвращает True если сайт запущен на локальном хосте
        /// </summary>
        public static bool IsLocalHost
        {
            get
            {
                if (HttpContext.Current != null)
                {
                    if (HttpContext.Current.Request != null)
                    {
                        if (Host.Contains("localhost"))
                        {
                            return true;
                        }
                    }
                }

                return false;
            }
        }

        /// <summary>
        ///   Возвращает IP
        /// </summary>
        public static string IP
        {
            get
            {
                if (HttpContext.Current != null)
                {
                    if (HttpContext.Current.Request != null)
                    {
                        return HttpContext.Current.Request.UserHostAddress;
                    }
                }

                return string.Empty;
            }
        }

        /// <summary>
        /// Безопасное обрезание IP (до 50 символов)
        /// </summary>
        public static string IPSafe
        {
            get
            {
                string ip = IP;
                if (string.IsNullOrEmpty(ip))
                    return ip;

                if (ip.Length > 50)
                    return ip.Substring(0, 50);
                return ip;
            }
        }

        public static string UrlReferrer
        {
            get
            {
                if ((HttpContext.Current != null) && (HttpContext.Current.Request != null))
                {
                    if (HttpContext.Current.Request.UrlReferrer != null)
                        return HttpContext.Current.Request.UrlReferrer.ToString();
                }

                return "/";
            }
        }

        /// <summary>
        /// True if current page is main page
        /// </summary>
        public static bool IsMainPage
        {
            get
            {
                if ((HttpContext.Current != null) && (HttpContext.Current.Request != null))
                {
                    return HttpContext.Current.Request.Url.PathAndQuery == "/";
                }

                return false;
            }
        }


        public static string UserAgent
        {
            get
            {
                if ((HttpContext.Current != null) && (HttpContext.Current.Request != null))
                {
                    return HttpContext.Current.Request.UserAgent;
                }
                return null;
            }
        }

        /// <summary>
        ///   Кодировать запрос
        /// </summary>
        public static string EncodeUrlString(this string input, Encoding encoding)
        {
            return HttpUtility.UrlEncode(input, encoding);
        }

        /// <summary>
        /// Return True if current UserAgent is Crawler
        /// </summary>
        public static bool IsCrawlerManual()
        {
            if (HttpContext.Current == null)
            {
                return false;
            }

            HttpRequest request = HttpContext.Current.Request;
            string userAgent = request.UserAgent;

            if (string.IsNullOrEmpty(userAgent))
                return false;


            return IsCrawlerManual(userAgent);
        }

        /// <summary>
        /// Return True if current UserAgent is Crawler
        /// </summary>
        public static bool IsCrawlerManual(string test)
        {
            return Regex.IsMatch(test,
                @"msnbot
        |www\.cuil\.com
    |Yahoo!\s+Slurp
    |Googlebot
    |Speedy\sSpider
    |MLBot
    |princeton crawler
    |accelobot
    |crawler\@dotnetdotcom
    |help\.naver\.com
    |GingerCrawler
    |Sosospider
    |www.exabot.com
    |Baiduspider
    |Ask\sJeeves
    |Java\/
    |telehouse\.ru
    |Tagoobot
    |Baypup
    |SimilarPages
    |Spinn3r
    |VoilaBot
    |Yandex
    |Xenu\sLink\sSleuth
    |www\.searchme\.com
    |MJ12bot
    |kilomonkey\.com
    |Mediapartners-Google
    |Sogou\sweb\sspider
    |YoudaoBot
    |seexie\.com
    |Yahoo.*Slurp
    |YahooCacheSystem
    |crawler\@nutch\.biz
    |psbot
    |ia_archiver-web\.archive\.org
    |sbider
    |xrss\.eu
    |scoutjet
        |www\.puritysearch\.net
        |Bing
        |BaiduImagespider
        |baidu\.jp
        |facebookexternalhit
        |ssllabs\.com
        |Python-urllib
        |drupal\.org
        |HTTrack
        |Willow\s+Internet\s+Crawler\s+by\s+Twotrees",
                RegexOptions.IgnoreCase | RegexOptions.Multiline | RegexOptions.IgnorePatternWhitespace);
        }
    }
}