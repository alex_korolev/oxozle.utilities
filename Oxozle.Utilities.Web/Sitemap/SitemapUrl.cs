﻿// *********************************************************************************
// Date: 	30.06.2014
// Project:	Oxozle.Utilities.Web.SitemapUrl.cs
//  
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// *********************************************************************************

#region Usings

using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;

#endregion

namespace Oxozle.Utilities.Web.Sitemap
{
    [Serializable]
    public class SitemapUrl
    {
        #region Properties

        private List<object> _properties = new List<object>();

        [XmlElement("changefreq", typeof (string))]
        [XmlElement("lastmod", typeof (DateTime))]
        [XmlElement("priority", typeof (float))]
        public List<object> Properties
        {
            get { return _properties; }
            set { _properties = value; }
        }

        [XmlElement(ElementName = "loc")]
        public string Location { get; set; }

        [XmlIgnore]
        public string ChangeFrequency
        {
            get { return _properties.Where(lm => lm.GetType().Equals(typeof (DateTime))).First() as string; }
            set
            {
                if (!String.IsNullOrEmpty(value))
                {
                    string changeFrequency =
                        _properties.Where(i => i.GetType().Equals(typeof (string))).FirstOrDefault() as string;
                    if (!String.IsNullOrEmpty(changeFrequency))
                    {
                        _properties.Remove(changeFrequency);
                    }
                    _properties.Add(value);
                }
            }
        }

        [XmlIgnore]
        public DateTime? LastModified
        {
            get { return _properties.Where(lm => lm.GetType().Equals(typeof (DateTime))).First() as DateTime?; }
            set
            {
                if (value.HasValue)
                {
                    DateTime? lastModified =
                        _properties.Where(i => i.GetType().Equals(typeof (DateTime))).FirstOrDefault() as DateTime?;
                    if (lastModified.HasValue)
                    {
                        _properties.Remove(lastModified.Value);
                    }
                    _properties.Add(value.Value);
                }
            }
        }

        [XmlIgnore]
        public float? Priority
        {
            get { return _properties.Where(lm => lm.GetType().Equals(typeof (DateTime))).FirstOrDefault() as float?; }
            set
            {
                if (value.HasValue)
                {
                    float? priority =
                        _properties.Where(i => i.GetType().Equals(typeof (float))).FirstOrDefault() as float?;
                    if (priority.HasValue)
                    {
                        _properties.Remove(priority.Value);
                    }
                    _properties.Add(value.Value);
                }
            }
        }

        #endregion
    }
}