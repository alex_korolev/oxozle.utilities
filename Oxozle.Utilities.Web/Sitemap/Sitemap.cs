﻿// *********************************************************************************
// Date: 	30.06.2014
// Project:	Oxozle.Utilities.Web.Sitemap.cs
//  
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// *********************************************************************************

#region Usings

using System;
using System.IO;
using System.Text;
using System.Web;
using System.Xml;
using System.Xml.Serialization;

#endregion

namespace Oxozle.Utilities.Web.Sitemap
{
    /// <summary>
    ///   Карта сайта
    /// </summary>
    public class Sitemap
    {
        private readonly Urlset set;
        private string baseUrl;

        public Sitemap()
        {
            set = new Urlset();
            baseUrl = String.Format("{0}://{1}/", HttpContext.Current.Request.Url.Scheme,
                HttpContext.Current.Request.Url.Authority);
        }

        public void Add(string url)
        {
            Add(url, null);
        }

        public void Add(string url, ChangeFrequency change)
        {
            if (change == null)
                Add(url, "", null);
            else
                Add(url, change.Value, null);
        }

        public void Add(string url, string change, float priority)
        {
            Add(url, change, null, priority);
        }

        public void Add(string url, string change, DateTime? lastModefied)
        {
            Add(url, change, lastModefied, null);
        }


        public void Add(string url, string change, DateTime? lastModefied, float? priority)
        {
            SitemapUrl current = new SitemapUrl();
            current.Location = url;
            current.ChangeFrequency = change;
            current.LastModified = lastModefied;
            current.Priority = priority;
            set.Urls.Add(current);
        }

        public void Add(SitemapUrl url)
        {
            set.Urls.Add(url);
        }

        public void Add(SitemapUrl[] urls)
        {
            foreach (SitemapUrl url in urls)
            {
                set.Urls.Add(url);
            }
        }

        public string GetSitemap()
        {
            // Finally, serialize and write it out to a UTF-8 encoded string so it can
            // be rendered to the search bot.
            MemoryStream memory = new MemoryStream();
            XmlTextWriter xmlWriter = new XmlTextWriter(memory, Encoding.UTF8);

            XmlSerializerNamespaces namespaces = new XmlSerializerNamespaces();
            namespaces.Add(String.Empty, "http://www.sitemaps.org/schemas/sitemap/0.9");

            XmlSerializer serializer = new XmlSerializer(typeof (Urlset), "http://www.sitemaps.org/schemas/sitemap/0.9");
            serializer.Serialize(xmlWriter, set, namespaces);

            return Encoding.UTF8.GetString(memory.ToArray());
        }
    }
}