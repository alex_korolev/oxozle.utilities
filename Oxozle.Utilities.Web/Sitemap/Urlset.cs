﻿// *********************************************************************************
// Date: 	30.06.2014
// Project:	Oxozle.Utilities.Web.Urlset.cs
//  
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// *********************************************************************************

#region Usings

using System;
using System.Collections.Generic;
using System.Xml.Serialization;

#endregion

namespace Oxozle.Utilities.Web.Sitemap
{
    [Serializable]
    [XmlRoot(ElementName = "urlset")]
    public class Urlset
    {
        private List<SitemapUrl> _urls = new List<SitemapUrl>();

        [XmlElement(ElementName = "url", Type = typeof (SitemapUrl))]
        public List<SitemapUrl> Urls
        {
            get { return _urls; }
            set { _urls = value; }
        }
    }
}