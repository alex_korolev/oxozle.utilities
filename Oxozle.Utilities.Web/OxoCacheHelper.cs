﻿// *********************************************************************************
// Author:	Azarov Dmitriy
// Email:	oxozle@gmail.com
// Date: 	30.06.2014
// Project:	Oxozle.Utilities.Web.OxoCacheHelper.cs
//  
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// *********************************************************************************

#region Usings

using System;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Caching;

#endregion

namespace Oxozle.Utilities.Web
{
    public static class OxoCacheHelper
    {
        /// <summary>
        /// Приоритет кеширования
        /// </summary>
        public static readonly CacheItemPriority CacheItemPriorityDefailt;


        static OxoCacheHelper()
        {
            CacheItemPriorityDefailt = CacheItemPriority.Default;
        }

        /// <summary>
        ///   Добавить элемент в кеш
        /// </summary>
        public static void Add(string key, object value, DateTime absoluteExpiration)
        {
            Add(key.ToUpper(), value, absoluteExpiration, Cache.NoSlidingExpiration);
        }


        /// <summary>
        ///   Добавить элемент в кеш
        /// </summary>
        public static void Add(string key, object value, DateTime absoluteExpiration, TimeSpan slidingExpiration)
        {
            Add(key.ToUpper(), value, null, absoluteExpiration, slidingExpiration, CacheItemPriorityDefailt);
        }

        /// <summary>
        ///   Добавить элемент в кеш
        /// </summary>
        /// <param name="key"> Ключ </param>
        /// <param name="value"> Значение </param>
        /// <param name="dependencies"> Зависимость </param>
        /// <param name="absoluteExpiration"> Дата истечения </param>
        /// <param name="slidingExpiration"></param>
        /// <param name="priority"> Приоритет </param>
        private static void Add(string key, object value, CacheDependency dependencies, DateTime absoluteExpiration,
            TimeSpan slidingExpiration, CacheItemPriority priority)
        {
            if (value == null)
            {
                value = DBNull.Value;
            }

            key = key.ToUpper();


            HttpRuntime.Cache.Insert(key, value, dependencies, absoluteExpiration, slidingExpiration, priority, null);
        }

        /// <summary>
        ///   Возвращает элемент из кеша
        /// </summary>
        /// <param name="key"> Ключ </param>
        public static object GetItem(string key)
        {
            key = key.ToUpper();

            object obj2 = HttpRuntime.Cache[key];
            if (obj2 == null)
            {
                return null;
            }
            if (obj2 == DBNull.Value)
            {
                obj2 = null;
            }

            return obj2;
        }

        /// <summary>
        ///   Удалить элемент
        /// </summary>
        /// <param name="key"> Ключ </param>
        public static void Remove(string key)
        {
            key = key.ToUpper();
            if (HttpRuntime.Cache[key] != null)
            {
                HttpRuntime.Cache.Remove(key);
            }
        }

        /// <summary>
        ///   Удалить все
        /// </summary>
        public static int Clear()
        {
            List<string> keys = new List<string>();

            // retrieve application Cache enumerator
            IDictionaryEnumerator enumerator = HttpRuntime.Cache.GetEnumerator();

            // copy all keys that currently exist in Cache
            while (enumerator.MoveNext())
            {
                keys.Add(enumerator.Key.ToString());
            }

            int keysCount = keys.Count;

            // delete every key from cache
            for (int i = 0; i < keys.Count; i++)
            {
                HttpRuntime.Cache.Remove(keys[i]);
            }

            return keysCount;
        }


        /// <summary>
        ///   Возвращает True если элемент есть в кеше
        /// </summary>
        /// <param name="key"> Ключ </param>
        public static bool Contains(string key)
        {
            key = key.ToUpper();
            return (HttpRuntime.Cache[key] != null);
        }
    }
}