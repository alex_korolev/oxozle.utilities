﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Oxozle.Utilities.Web
{
    public static class OxoQueryString
    {
        /// <summary>
        ///   Адрес для разбиения на страницы
        /// </summary>
        /// <returns> </returns>
        public static string GetBasePagedUrl()
        {
            string url = RemoveParameterFromUrl(OxoQuery.RawUrl, "page");
            return AddParameterToUrl(url, "page", "");
        }


        /// <summary>
        ///   Добавляет параметр в URL
        /// </summary>
        /// <param name="url"> Исходный URL </param>
        /// <param name="parameterName"> Имя параметра </param>
        /// <param name="parameterValue"> Значение параметра </param>
        /// <returns> Новый URL </returns>
        public static string AddParameterToUrl(this string url, string parameterName, string parameterValue)
        {
            if (url == null)
            {
                return null;
            }
            string queryStringValue = "";
            int index = url.IndexOf("?");
            if (index >= 0)
            {
                queryStringValue = url.Substring(index);
                url = url.Substring(0, index);
            }
            if (!string.IsNullOrEmpty(parameterName))
            {
                queryStringValue = AddUrlParameter(queryStringValue, parameterName, parameterValue);
            }
            return (url + queryStringValue);
        }

        /// <summary>
        ///   Удаляет параметр из УРЛ
        /// </summary>
        /// <param name="url"> УРЛ </param>
        /// <param name="parameterName"> Имя параметра </param>
        public static string RemoveParameterFromUrl(this string url, string parameterName)
        {
            if ((HttpContext.Current == null) || (HttpContext.Current.Request == null))
            {
                return null;
            }
            Uri uri = null;
            try
            {
                uri = new Uri(url);
            }
            catch
            {
                url = HttpContext.Current.Request.Url.Scheme + "://" + GetFullDomain() + url;
                uri = new Uri(url);
            }
            string str = RemoveUrlParameter(uri.Query, parameterName);
            //return (uri.GetLeftPart(UriPartial.Path) + str);
            return (uri.LocalPath + str);
        }

        /// <summary>
        ///   Обновляет параметр в УРЛ. Удаляет все значение и записывает новыое
        /// </summary>
        public static string UpdateParameterInUrl(this string url, string parameterName, string parameterValue)
        {
            url = RemoveParameterFromUrl(url, parameterName);
            return AddParameterToUrl(url, parameterName, parameterValue);
        }

        /// <summary>
        ///   Удаляет значение из урл
        /// </summary>
        public static string DeleteParameterValue(this string url, string parameterName, string parameterValue)
        {
            if (url == null)
                return null;
            string queryStringValue = "";
            int index = url.IndexOf("?");
            if (index >= 0)
            {
                queryStringValue = url.Substring(index);
                url = url.Substring(0, index);
            }
            if (!string.IsNullOrEmpty(parameterName))
            {
                queryStringValue = DeleteUrlParameterValue(queryStringValue, parameterName, parameterValue);
            }
            return url + queryStringValue;
        }


        /// <summary>
        ///   Удаляет параметр из УРЛ
        /// </summary>
        /// <param name="queryStringValue"> Запрос </param>
        /// <param name="keyName"> Имя </param>
        private static string RemoveUrlParameter(string queryStringValue, string keyName)
        {
            if (queryStringValue.IsEmpty())
            {
                queryStringValue = "";
                return queryStringValue;
            }
            
            string str = "";
            if (queryStringValue.IndexOf("?") < 0)
            {
                return queryStringValue;
            }
            string str2 = queryStringValue.Substring(queryStringValue.IndexOf("?"));
            if (str2.StartsWith("?"))
            {
                str2 = str2.Remove(0, 1);
            }
            string[] strArray = str2.Split(new[] { '&' });
            for (int i = 0; i <= strArray.GetUpperBound(0); i++)
            {
                string[] strArray2 = strArray[i].Split(new[] { '=' });

                if (((strArray2 != null) && (strArray2.Length > 1)) && (strArray2[0].ToLower() != keyName.ToLower()))
                {
                    if ((str != null) && (str.Trim() != ""))
                    {
                        str = str + "&";
                    }

                    str = str + strArray2[0] + "=" + strArray2[1];
                }
            }
            if (str == "")
            {
                return str;
            }
            return ("?" + str);
        }

        /// <summary>
        ///   Добавляет параметр в URL. Если такой параметр уже есть, то добавляет новый через запятую
        /// </summary>
        /// <param name="queryStringValue"> УРЛ </param>
        /// <param name="newKeyName"> Имя </param>
        /// <param name="newKeyValue"> Значение </param>
        /// <returns> Новый УРЛ </returns>
        private static string AddUrlParameter(string queryStringValue, string newKeyName, string newKeyValue)
        {
            if (queryStringValue == null)
            {
                queryStringValue = "";
            }
            if (string.IsNullOrEmpty(newKeyName))
            {
                return queryStringValue;
            }
            if (queryStringValue.IndexOf("?") < 0)
            {
                return ("?" + newKeyName + "=" + newKeyValue);
            }
            string str = queryStringValue.Substring(queryStringValue.IndexOf("?"));
            if (str.StartsWith("?"))
            {
                str = str.Remove(0, 1);
            }
            bool flag = false;
            string[] strArray = str.Split(new[] { '&' });
            string str2 = "";
            for (int i = 0; i <= strArray.GetUpperBound(0); i++)
            {
                string[] strArray2 = strArray[i].Split(new[] { '=' });
                if ((strArray2 != null) && (strArray2.Length > 1))
                {
                    if (strArray2[0].ToLower() != newKeyName.ToLower())
                    {
                        if (i > 0)
                        {
                            str2 = str2 + "&";
                        }
                        str2 = str2 + strArray2[0] + "=" + strArray2[1];
                    }
                    else
                    {
                        flag = true;
                        if (i > 0)
                        {
                            str2 = str2 + "&";
                        }

                        //added by klubs to improve this part
                        string[] values = strArray2[1].Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        string addedParameter = "";
                        if (values.Length > 0)
                        {
                            for (int j = 0; j < values.Length; j++)
                            {
                                if (values[j].ToLower() != newKeyValue.ToLower())
                                {
                                    addedParameter += values[j] + ",";
                                }
                            }

                            addedParameter += newKeyValue;
                        }
                        else
                        {
                            addedParameter = newKeyValue;
                        }
                        str2 = str2 + newKeyName + "=" + addedParameter;
                        //end added
                    }
                }
            }
            if (!flag)
            {
                if (strArray.Length > 0)
                {
                    str2 = str2 + "&";
                }
                str2 = str2 + newKeyName + "=" + newKeyValue;
            }
            if (str2 == "")
            {
                return str2;
            }
            return ("?" + str2);
        }

        /// <summary>
        ///   Удаляет одно значение из параметров URL
        /// </summary>
        private static string DeleteUrlParameterValue(string queryStringValue, string parameterName,
                                                      string parameterValueToDelete)
        {
            if (queryStringValue == null)
                queryStringValue = "";

            //Если у нас в строке и так ничего нет
            if (queryStringValue.IndexOf("?") < 0)
                return string.Empty;

            //Получаем все что до вопроса
            string queryFromQuestion = queryStringValue.Substring(queryStringValue.IndexOf("?"));
            if (queryFromQuestion.StartsWith("?"))
                queryFromQuestion = queryFromQuestion.Remove(0, 1);


            //bool flag = false;
            string[] queryParameters = queryFromQuestion.Split(new[] { '&' });

            //В эту переменную строится новый запрос
            string resultQueryBuild = "";


            for (int i = 0; i <= queryParameters.GetUpperBound(0); i++)
            {
                string[] currentParameterValue = queryParameters[i].Split(new[] { '=' });
                if ((currentParameterValue != null) && (currentParameterValue.Length > 1))
                {
                    //Если это не тот параметр, значение которого мы собираемся обновлять
                    //То просто записываем его
                    if (currentParameterValue[0].ToLower() != parameterName.ToLower())
                    {
                        //Если это не первый параметр, то нужно ставить апресант
                        if (i > 0)
                            resultQueryBuild = resultQueryBuild + "&";

                        resultQueryBuild = resultQueryBuild + currentParameterValue[0] + "=" + currentParameterValue[1];
                    }
                    //Наш параметр, его то и надо обновить
                    else
                    {
                        string[] values = currentParameterValue[1].Split(new[] { ',' },
                                                                         StringSplitOptions.RemoveEmptyEntries);

                        if (values.Length == 0)
                            continue;

                        if (values.Length == 1 && values[0] == parameterValueToDelete)
                            continue;

                        string addedParameter = "";
                        for (int j = 0; j < values.Length; j++)
                            if (values[j].ToLower() != parameterValueToDelete.ToLower())
                                addedParameter += values[j] + ",";

                        if (addedParameter.Length > 1)
                            addedParameter = addedParameter.Substring(0, addedParameter.Length - 1);


                        //flag = true;
                        if (i > 0)
                            resultQueryBuild = resultQueryBuild + "&";

                        resultQueryBuild = resultQueryBuild + currentParameterValue[0] + "=" + addedParameter;
                    }
                }
            }
            //if (!flag)
            //{
            //    if (queryParameters.Length > 0)
            //    {
            //        resultQueryBuild = resultQueryBuild + "&";
            //    }
            //    resultQueryBuild = resultQueryBuild + newKeyName + "=" + newKeyValue;
            //}
            if (resultQueryBuild == "")
            {
                return resultQueryBuild;
            }
            return ("?" + resultQueryBuild);
        }


        /// <summary>
        ///   Возвращает полный домен
        /// </summary>
        public static string GetFullDomain()
        {
            if ((HttpContext.Current == null) || (HttpContext.Current.Request == null))
            {
                return OxoValidation.GetString(OxoHttpContextRequestStock.GetItem("FullDomain"), "");
            }
            string authority = HttpContext.Current.Request.Url.Authority;
            if (authority.EndsWith(":80"))
            {
                authority = authority.Substring(0, authority.Length - 3);
            }
            return authority;
        }
    }
}
