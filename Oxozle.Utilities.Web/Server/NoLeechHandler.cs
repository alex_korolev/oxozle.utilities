﻿// *********************************************************************************
// Author:	Azarov Dmitriy
// Email:	oxozle@gmail.com
// Date: 	30.06.2014
// Project:	Oxozle.Utilities.Web.NoLeechHandler.cs
//  
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// *********************************************************************************

#region Usings

using System.Globalization;
using System.IO;
using System.Web;

#endregion

namespace Oxozle.Utilities.Web.Server
{
    /*  <system.web>
            <httpHandlers>
                <add path="*.docx" verb="*" type="Oxozle.Utilities.Web.MVC.NoLeechHandler" />
            </httpHandlers>
        </system.web>
     * 
     * 
     * 
        <system.webServer>
            <handlers>
                <add path="*.docx" verb="*" name="NoLeechHandler" type="Oxozle.Utilities.Web.MVC.NoLeechHandler" />
            </handlers>
        </system.webServer>
     */

    /// <summary>
    /// Хендлер, запрещяющий прямое скачивание документов doc + docx
    /// </summary>
    public class NoLeechHandler : IHttpHandler
    {
        #region IHttpModule Members

        public void ProcessRequest(HttpContext ctx)
        {
            HttpRequest req = ctx.Request;
            string path = req.PhysicalPath;
            string extension = null;

            if (req.UrlReferrer != null && req.UrlReferrer.Host.Length > 0)
            {
                if (
                    CultureInfo.InvariantCulture.CompareInfo.Compare(req.Url.Host, req.UrlReferrer.Host,
                        CompareOptions.IgnoreCase) != 0)
                {
                    ctx.Response.Write("Keep On Dreaming");
                    return;
                }
            }


            string contentType = null;
            extension = Path.GetExtension(path).ToLower();
            switch (extension)
            {
                case ".doc":
                case ".docx":
                    ctx.Response.Write("Keep On Dreaming");
                    break;
            }
        }

        public bool IsReusable
        {
            get { return true; }
        }

        #endregion
    }
}