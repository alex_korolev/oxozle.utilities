﻿// *********************************************************************************
// Author:	Azarov Dmitriy
// Email:	oxozle@gmail.com
// Date: 	30.06.2014
// Project:	Oxozle.Utilities.Web.OxoPointAntiSpam.cs
//  
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// *********************************************************************************

#region Usings

using System;
using System.Text.RegularExpressions;
using System.Web;

#endregion

namespace Oxozle.Utilities.Web.AntiSpam
{
    /// <summary>
    ///   Класс для анализа текстовых сообщений на предмет спама. Используется метод "Очков". 
    ///   Встроены дополнииельные методы для более точного определения сообщения как спам.
    /// </summary>
    public sealed class OxoPointAntiSpam
    {
        /// <summary>
        ///   Нормальная длина сообщения. Если текст больше этой длины то прибавляется 2 очка, если меньше от отнимается.
        ///   Используется как настройка по умаолчнию
        /// </summary>
        private const int NormalMessageLength = 20;

        /// <summary>
        ///   Количество очков в сумме за все проверки. По умолчанию 0.
        /// </summary>
        private int _points;

        /// <summary>
        ///   Количество очков
        /// </summary>
        public int Points
        {
            get { return _points; }
        }

        /// <summary>
        ///   Возвращает True если сообщение - спам
        /// </summary>
        public bool IsSpam
        {
            get { return Points <= 0; }
        }

        /// <summary>
        ///   Проверяет UrlReferrer. Т.е. откуда пришел запрос. Если UrlReferrer пустой, значит запрос пришел просто так "на лету" а это признак спама или бота.
        ///   Если UrlReferrer не совпадает с
        /// </summary>
        /// <param name = "context"></param>
        public void ValidateRequest(HttpContext context)
        {
            if (context == null)
                return;

            if (context.Request == null)
                return;

            if ((context.Request.UrlReferrer == null) ||
                (!context.Request.UrlReferrer.Authority.Equals(context.Request.Url.Authority,
                    StringComparison.InvariantCultureIgnoreCase)))
                _points -= 2;
        }

        /// <summary>
        ///   Есть ли в сообщении протокол. Например можно проверять есть ли протокол в имени или других полях где он 100% не может быть
        /// </summary>
        /// <param name = "message">Сообщение для проверки</param>
        public void HaveHttp(string message)
        {
            if (message.IndexOf("http", StringComparison.OrdinalIgnoreCase) != -1)
                _points -= 2;
        }

        /// <summary>
        ///   Длина сообщение. Проверяет длину сообщение на минимальную адекватную. По умолчанию используется
        ///   NormalMessageLength
        /// </summary>
        /// <param name = "message">Сообщение для проверки</param>
        public void Length(string message)
        {
            Length(message, NormalMessageLength);
        }

        /// <summary>
        ///   Длина сообщение. Проверяет длину сообщение на минимальную адекватную.
        /// </summary>
        /// <param name = "message">Сообщение для проверки</param>
        /// <param name = "normalLength">Нормальная длина сообщения. (Зависит от ситуации)</param>
        public void Length(string message, int normalLength)
        {
            if (string.IsNullOrEmpty(message))
            {
                _points -= 2;
                return;
            }

            if (message.Length < normalLength)
            {
                _points -= 1;
                return;
            }

            _points += 2;
        }

        /// <summary>
        ///   Количество ссылок в сообщении. Чем больше ссылок тем больше вероятность что это спам
        /// </summary>
        /// <param name = "message"></param>
        public void LinksCount(string message)
        {
            Regex httpRegex = new Regex("http://", RegexOptions.IgnoreCase);
            MatchCollection mc = httpRegex.Matches(message);

            if (mc.Count > 2)
            {
                _points -= 2;
                return;
            }

            _points += 1;
        }

        /// <summary>
        ///   Поиск русских Букв. Если процентное содержание русских букв меньше 10, то большая вероятность что это сообщение - спам
        /// </summary>
        /// <param name = "message"></param>
        public void IsRussian(string message)
        {
            if (string.IsNullOrEmpty(message))
                _points -= 1;

            int minBig = 'А';
            int maxSmall = 'я';
            int length = message.Length;
            int foundRussian = 0;

            foreach (char c in message)
            {
                int currentChar = c;

                if (
                    (currentChar >= minBig && currentChar <= maxSmall))
                    foundRussian++;
            }

            if ((foundRussian*100/length) < 10)
            {
                _points -= 2;
                return;
            }

            _points += 1;
        }

        /// <summary>
        ///   Находит ссылки в формате BB. Если ссылки есть (а их не должно быть) это спам
        ///   http://www.dreamincode.net/code/snippet2661.htm
        /// </summary>
        /// <param name = "message"></param>
        public void HasBBTags(string message)
        {
            if (string.IsNullOrEmpty(message))
                return;

            MatchCollection mc;

            Regex urlRegex = new Regex(@"\[url\=([^\]]+)\]([^\]]+)\[/url\]");
            mc = urlRegex.Matches(message);

            if (mc.Count > 0)
                _points -= 2;

            Regex linkRegex = new Regex(@"\[link\=([^\]]+)\]([^\]]+)\[/link\]");
            mc = linkRegex.Matches(message);

            if (mc.Count > 0)
                _points -= 2;
        }
    }
}