﻿// *********************************************************************************
// Author:	Azarov Dmitriy
// Email:	oxozle@gmail.com
// Date: 	30.06.2014
// Project:	Oxozle.Utilities.Web.OxoAntiSpamService.cs
//  
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// *********************************************************************************

#region Usings

#endregion

namespace Oxozle.Utilities.Web.AntiSpam
{
    public sealed class OxoAntiSpamService
    {
        private readonly OxoPointAntiSpam _antiSpam;

        public OxoAntiSpamService()
        {
            _antiSpam = new OxoPointAntiSpam();
        }


        /// <summary>
        ///   Проверка комментария
        /// </summary>
        public int ValidateComment(string Name, string Email, string Comment)
        {
            //Есть теги
            _antiSpam.HasBBTags(Comment);

            //Имя русское
            _antiSpam.IsRussian(Name);

            _antiSpam.IsRussian(Comment);

            //Есть ссылки
            _antiSpam.HaveHttp(Comment);

            return _antiSpam.Points;
        }
    }
}