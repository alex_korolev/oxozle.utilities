﻿// *********************************************************************************
// Author:	Azarov Dmitriy
// Email:	oxozle@gmail.com
// Date: 	30.06.2014
// Project:	Oxozle.Utilities.Web.MVC.OxoHtmlHelper.cs
//  
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// *********************************************************************************

#region Usings

using System;
using System.Text;
using System.Web;
using System.Web.Mvc;

#endregion

namespace Oxozle.Utilities.Web
{
    public static class OxoHtmlHelper
    {
        private static readonly Random _rand;

        static OxoHtmlHelper()
        {
            _rand = new Random();
        }

        /// <summary>
        /// Возвращает True, если выпала вероятность показа
        /// </summary>
        /// <param name="chance">Вероятность события в %</param>
        /// <returns></returns>
        public static bool ShowContent(int chance)
        {
            int currentChange = _rand.Next(100);
            if (chance < currentChange)
                return true;

            return false;
        }

        public static HtmlString DropDown(SelectList selectList, string name, string id, string classValue = null)
        {
            StringBuilder sb = new StringBuilder();

            string append = string.Empty;
            if (classValue != null)
                append = " class=\"" + classValue + "\"";

            sb.AppendLine("<select name=\"" + name + "\" id=\"" + id + "\"" + append + " >");

            foreach (SelectListItem selectListItem in selectList)
            {
                if (selectListItem.Selected)
                {
                    sb.AppendLine("<option selected=\"selected\" value=\"" + selectListItem.Value + "\">" +
                                  selectListItem.Text + "</option>");
                }
                else
                {
                    sb.AppendLine("<option value=\"" + selectListItem.Value + "\">" + selectListItem.Text + "</option>");
                }
            }

            sb.AppendLine("</select>");
            return new HtmlString(sb.ToString());
        }

        /// <summary>
        /// Чек бокс, но ссылка
        /// </summary>
        public static HtmlString CheckLink(string url, string text, bool isChecked)
        {
            return new
                HtmlString("<a href=\"" + url + "\" class=\"" + (isChecked
                    ? "checked"
                    : "unchecked") + "\">" + text + "</a>");
        }

        public static HtmlString RadioLink(string url, string text, bool isChecked)
        {
            return new
                HtmlString("<a href=\"" + url + "\" class=\"" + (isChecked
                    ? "r-checked"
                    : "r-unchecked") + "\">" + text + "</a>");
        }

        /// <summary>
        /// Конвертирует перевод строки в разрывы HTML
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static MvcHtmlString Nl2Br(this string text)
        {
            StringBuilder builder = new StringBuilder();
            string[] lines = text.Split('\n');
            for (int i = 0; i < lines.Length; i++)
            {
                if (i > 0)
                    builder.Append("<br/>");
                builder.Append(HttpUtility.HtmlEncode(lines[i]));
            }
            return MvcHtmlString.Create(builder.ToString());
        }


        public static HtmlString TagStart(string tagName)
        {
            return new HtmlString("<" + tagName + ">");
        }

        public static HtmlString TagEnd(string tagName)
        {
            return new HtmlString("</" + tagName + ">");
        }


        /// <summary>
        /// Чек Бокс со значением
        /// </summary>
        public static HtmlString ValuableCheckBox(string name, short value, bool ischecked)
        {
            if (ischecked)
                return new HtmlString("<input type=\"checkbox\"  name=\"" + name +
                                      "\" value=\"" + value + "\" checked=\"checked\" />");

            return new HtmlString("<input type=\"checkbox\"  name=\"" + name +
                                  "\" value=\"" + value + "\" />");
        }

        /// <summary>
        /// Чек Бокс со значением
        /// </summary>
        public static HtmlString ValuableCheckBox(string name, int value, bool ischecked)
        {
            if (ischecked)
                return new HtmlString("<input type=\"checkbox\"  name=\"" + name +
                                      "\" value=\"" + value + "\" checked=\"checked\" />");

            return new HtmlString("<input type=\"checkbox\"  name=\"" + name +
                                  "\" value=\"" + value + "\" />");
        }
    }
}