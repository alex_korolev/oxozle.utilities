﻿// *********************************************************************************
// Author:	Azarov Dmitriy
// Email:	oxozle@gmail.com
// Date: 	30.06.2014
// Project:	Oxozle.Utilities.Win.WorkRequest.cs
//  
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// *********************************************************************************

namespace Oxozle.Utilities.Win.AsyncPool
{
    /// <summary>
    /// Async work Input Data
    /// </summary>
    public class WorkRequest
    {
        public WorkRequest()
        {
            UIWorkRequest = new UIWorkRequest();
        }

        public WorkRequest(object obj)
        {
            UIWorkRequest = new UIWorkRequest();
            WorkObject = obj;
        }

        public WorkRequest(object obj, string updateUI)
        {
            UIWorkRequest = new UIWorkRequest {UIWorkName = updateUI};
            WorkObject = obj;
        }

        /// <summary>
        /// UI Update request
        /// </summary>
        public UIWorkRequest UIWorkRequest { get; set; }

        /// <summary>
        /// Data
        /// </summary>
        public object WorkObject { get; set; }

        public static WorkRequest Create(string workName)
        {
            return Create(workName, null, null);
        }

        public static WorkRequest Create(string workName, string description, object workObject)
        {
            WorkRequest request = new WorkRequest();
            request.UIWorkRequest.UIWorkName = workName;
            request.UIWorkRequest.UIWorkDescription = description;
            request.WorkObject = workObject;

            return request;
        }
    }
}