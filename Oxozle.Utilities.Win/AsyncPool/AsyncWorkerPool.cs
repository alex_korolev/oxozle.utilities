﻿// *********************************************************************************
// Author:	Azarov Dmitriy
// Email:	oxozle@gmail.com
// Date: 	30.06.2014
// Project:	Oxozle.Utilities.Win.AsyncWorkerPool.cs
//  
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// *********************************************************************************

#region Usings

using System;
using System.Collections.Generic;

#endregion

namespace Oxozle.Utilities.Win.AsyncPool
{
    /// <summary>
    /// Async worker pool
    /// </summary>
    public class AsyncWorkerPool : IDisposable
    {
        /// <summary>
        /// Worker count
        /// </summary>
        private readonly int _capacity;

        private readonly object _locker = new object();

        /// <summary>
        /// workers
        /// </summary>
        private readonly AsyncWorker[] _workers;

        /// <summary>
        /// jobs
        /// </summary>
        private readonly Queue<AsyntWork> _works;

        /// <summary>
        /// Create work pool
        /// </summary>
        /// <param name = "capacity">Workers count</param>
        public AsyncWorkerPool(int capacity)
        {
            _capacity = capacity;
            _works = new Queue<AsyntWork>();

            _workers = new AsyncWorker[_capacity];
            for (int i = 0; i < _capacity; i++)
            {
                _workers[i] = new AsyncWorker();
                _workers[i].OnWorkComplete += RunNewWork;
            }
        }

        public void Dispose()
        {
            for (int i = 0; i < _capacity; i++)
            {
                _workers[i].OnWorkComplete -= RunNewWork;
                _workers[i].Dispose();
            }
        }

        /// <summary>
        /// When somebody ends work we need start new one.
        /// </summary>
        private void RunNewWork(object sender, EventArgs e)
        {
            bool runAgain = false;

            lock (_locker)
            {
                if (_works.Count > 0)
                {
                    for (int i = 0; i < _capacity; i++)
                    {
                        if (!_workers[i].IsWork)
                        {
                            _workers[i].RunAsync(_works.Dequeue());
                            runAgain = true;
                            goto Started;
                        }
                    }
                }
            }

            Started:
            if (runAgain)
                RunNewWork(null, null);
        }

        #region Add Work

        public void AddWork(Func<WorkRequest, WorkResponse> function)
        {
            AddWork(function, null);
        }

        public void AddWork(Func<WorkRequest, WorkResponse> function, Action<WorkResponse> Callback)
        {
            AddWork(function, Callback, null);
        }

        public void AddWork(Func<WorkRequest, WorkResponse> function, Action<WorkResponse> Callback,
            Action<UIWorkRequest> StartWork)
        {
            AddWork(function, Callback, StartWork, null);
        }

        public void AddWork(Func<WorkRequest, WorkResponse> function, Action<WorkResponse> Callback,
            Action<UIWorkRequest> StartWork, WorkRequest request)
        {
            AsyntWork work = new AsyntWork();
            work.Work = function;
            work.Callback = Callback;
            work.UpdateStartWork = StartWork;
            work.WorkRequest = request;

            _works.Enqueue(work);

            RunNewWork(null, null);
        }

        #endregion
    }
}