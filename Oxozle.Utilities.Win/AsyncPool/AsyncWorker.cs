﻿// *********************************************************************************
// Author:	Azarov Dmitriy
// Email:	oxozle@gmail.com
// Date: 	30.06.2014
// Project:	Oxozle.Utilities.Win.AsyncWorker.cs
//  
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// *********************************************************************************

#region Usings

using System;
using System.ComponentModel;

#endregion

namespace Oxozle.Utilities.Win.AsyncPool
{
    /// <summary>
    /// Async worker (encapsulate BackgroundWorker)
    /// </summary>
    public class AsyncWorker : IDisposable
    {
        /// <summary>
        /// worker
        /// </summary>
        private readonly BackgroundWorker _worker;

        /// <summary>
        /// Work data request
        /// </summary>
        private AsyntWork _work;

        public AsyncWorker()
        {
            _worker = new BackgroundWorker();
            _worker.DoWork += WorkerDoWork;
            _worker.RunWorkerCompleted += WorkerRunWorkerCompleted;
            _worker.ProgressChanged += OnWorkStart;
            _worker.WorkerReportsProgress = true;
        }

        /// <summary>
        /// True, when worker is busy
        /// </summary>
        public bool IsWork
        {
            get { return _worker.IsBusy; }
        }

        public void Dispose()
        {
            if (_worker != null)
            {
                if (_worker.IsBusy)
                {
                    //note mmmm...
                }

                _worker.DoWork -= WorkerDoWork;
                _worker.RunWorkerCompleted -= WorkerRunWorkerCompleted;
                _worker.ProgressChanged -= OnWorkStart;

                _worker.Dispose();
            }
        }

        /// <summary>
        /// Event wher All work done
        /// </summary>
        public event EventHandler OnWorkComplete;


        public void RunAsync(Func<WorkRequest, WorkResponse> function)
        {
            RunAsync(function, null);
        }

        public void RunAsync(Func<WorkRequest, WorkResponse> function, Action<WorkResponse> Callback)
        {
            RunAsync(function, Callback, null);
        }

        public void RunAsync(Func<WorkRequest, WorkResponse> function, Action<WorkResponse> Callback,
            Action<UIWorkRequest> StartWork)
        {
            RunAsync(function, Callback, StartWork, null);
        }

        public void RunAsync(Func<WorkRequest, WorkResponse> function, Action<WorkResponse> Callback,
            Action<UIWorkRequest> StartWork, WorkRequest request)
        {
            AsyntWork work = new AsyntWork();
            work.Work = function;
            work.Callback = Callback;
            work.UpdateStartWork = StartWork;
            work.WorkRequest = request;

            RunAsync(work);
        }

        public void RunAsync(AsyntWork work)
        {
            _work = work;
            _worker.RunWorkerAsync();
        }

        /// <summary>
        /// On work start
        /// </summary>
        private void OnWorkStart(object sender, ProgressChangedEventArgs e)
        {
            if (_work.UpdateStartWork != null)
                _work.UpdateStartWork(e.UserState as UIWorkRequest);
        }

        /// <summary>
        /// On work end
        /// </summary>
        private void WorkerRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //Сначала уведомим рабочую программу
            if (_work.Callback != null)
                _work.Callback(_work.WorkResponse);

            //А потом всех остальных
            if (OnWorkComplete != null)
                OnWorkComplete(this, null);
        }

        /// <summary>
        /// Please, do work!
        /// </summary>
        private void WorkerDoWork(object sender, DoWorkEventArgs e)
        {
            if (_work.WorkRequest != null)
                _worker.ReportProgress(0, _work.WorkRequest.UIWorkRequest);

            _work.WorkResponse = new WorkResponse();
            _work.WorkResponse = _work.Work(_work.WorkRequest);
        }
    }
}