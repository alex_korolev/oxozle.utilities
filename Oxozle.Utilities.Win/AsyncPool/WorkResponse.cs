﻿// *********************************************************************************
// Author:	Azarov Dmitriy
// Email:	oxozle@gmail.com
// Date: 	30.06.2014
// Project:	Oxozle.Utilities.Win.WorkResponse.cs
//  
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// *********************************************************************************

namespace Oxozle.Utilities.Win.AsyncPool
{
    /// <summary>
    /// Async work result
    /// </summary>
    public class WorkResponse
    {
        public WorkResponse()
        {
        }

        public WorkResponse(object response)
        {
            WorkResult = response;
        }

        /// <summary>
        /// Data
        /// </summary>
        public object WorkResult { get; set; }

        public T GetValue<T>()
        {
            return (T) WorkResult;
        }
    }
}