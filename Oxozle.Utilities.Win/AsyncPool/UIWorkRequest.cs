﻿// *********************************************************************************
// Author:	Azarov Dmitriy
// Email:	oxozle@gmail.com
// Date: 	30.06.2014
// Project:	Oxozle.Utilities.Win.UIWorkRequest.cs
//  
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// *********************************************************************************

namespace Oxozle.Utilities.Win.AsyncPool
{
    /// <summary>
    /// UI update
    /// </summary>
    public class UIWorkRequest
    {
        /// <summary>
        /// Work title
        /// </summary>
        public string UIWorkName { get; set; }

        /// <summary>
        /// Work description
        /// </summary>
        public string UIWorkDescription { get; set; }

        /// <summary>
        /// Progress percent
        /// </summary>
        public int UIProgressValue { get; set; }
    }
}