﻿// *********************************************************************************
// Author:	Azarov Dmitriy
// Email:	oxozle@gmail.com
// Date: 	30.06.2014
// Project:	Oxozle.Utilities.Win.AsyncWork.cs
//  
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// *********************************************************************************

#region Usings

using System;

#endregion

namespace Oxozle.Utilities.Win.AsyncPool
{
    /// <summary>
    /// Async work object
    /// </summary>
    public class AsyntWork
    {
        /// <summary>
        /// Callback when work completedработы)
        /// </summary>
        public Action<WorkResponse> Callback;

        /// <summary>
        /// Callback for UI update
        /// </summary>
        public Action<UIWorkRequest> UpdateStartWork;

        /// <summary>
        /// Async work method
        /// </summary>
        public Func<WorkRequest, WorkResponse> Work;

        /// <summary>
        /// Async work Input Data
        /// </summary>
        public WorkResponse WorkResponse { get; set; }

        /// <summary>
        /// Async work Output Data
        /// </summary>
        public WorkRequest WorkRequest { get; set; }
    }
}