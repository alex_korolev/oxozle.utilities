﻿// *********************************************************************************
// Author:	Azarov Dmitriy
// Email:	oxozle@gmail.com
// Date: 	30.06.2014
// Project:	Oxozle.Utilities.Win.OSHelper.cs
//  
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// *********************************************************************************

#region Usings

using System;
using System.Diagnostics;
using System.IO;
using System.Security.Principal;

#endregion

namespace Oxozle.Utilities.Win.System
{
    public static class OSHelper
    {
        /// <summary>
        ///   Навигация в URL
        /// </summary>
        public static void NavigateURL(string url)
        {
            try
            {
                Process.Start(url);
            }
            catch (Exception exception)
            {
            }
        }

        /// <summary>
        ///   Запустить процесс
        /// </summary>
        public static void RunProcessSafe(string path)
        {
            RunProcessSafe(path, null);
        }

        /// <summary>
        ///   Запустить процесс
        /// </summary>
        public static void RunProcessSafe(string path, string args)
        {
            if (File.Exists(path))
            {
                Process.Start(path, args);
            }
        }

        public static bool IsUserAdministrator()
        {
            //bool value to hold our return value
            bool isAdmin;
            try
            {
                //get the currently logged in user
                WindowsIdentity user = WindowsIdentity.GetCurrent();
                WindowsPrincipal principal = new WindowsPrincipal(user);
                isAdmin = principal.IsInRole(WindowsBuiltInRole.Administrator);
            }
            catch (UnauthorizedAccessException ex)
            {
                isAdmin = false;
            }
            catch (Exception ex)
            {
                isAdmin = false;
            }
            return isAdmin;
        }
    }
}