﻿// *********************************************************************************
// Author:	Azarov Dmitriy
// Email:	oxozle@gmail.com
// Date: 	01.07.2014
// Project:	Oxozle.Utilities.Win.ILogger.cs
//  
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// *********************************************************************************

#region Usings

using System;

#endregion

namespace Oxozle.Utilities.Win.Logger
{
    public interface ILogger
    {
        ///// <summary>
        ///// The log name
        ///// </summary>
        //string LogName { get; }

        /// <summary>
        /// Сообщение которое необходимо только для отладки 
        /// и просмотра состояния.
        /// </summary>
        void DebugMessage(string message);

        /// <summary>
        /// Add information for the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        void Information(string message);


        /// <summary>
        /// Add warning for the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        void Warning(string message);


        /// <summary>
        /// Add error for the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        void Error(string message);


        /// <summary>
        /// Add errors for the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="exception">The exception.</param>
        void Error(string message, Exception exception);
    }
}