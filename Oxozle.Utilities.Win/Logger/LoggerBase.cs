﻿// *********************************************************************************
// Author:	Azarov Dmitriy
// Email:	oxozle@gmail.com
// Date: 	01.07.2014
// Project:	Oxozle.Utilities.Win.LoggerBase.cs
//  
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// *********************************************************************************

#region Usings

using System;

#endregion

namespace Oxozle.Utilities.Win.Logger
{
    public abstract class LoggerBase : ILogger
    {
        protected string _applicationName;

        protected LoggerBase(string applicationName)
        {
            _applicationName = applicationName;
        }

        //public string LogName { get; private set; }

        public abstract void DebugMessage(string message);

        public abstract void Information(string message);

        public abstract void Warning(string message);


        public abstract void Error(string message);


        public abstract void Error(string message, Exception exception);
    }
}