﻿// *********************************************************************************
// Author:	Azarov Dmitriy
// Email:	oxozle@gmail.com
// Date: 	01.07.2014
// Project:	Oxozle.Utilities.Win.EventLogger.cs
//  
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// *********************************************************************************

#region Usings

using System;
using System.Diagnostics;
using System.Text;

#endregion

namespace Oxozle.Utilities.Win.Logger
{
    /// <summary>
    /// Local class for log events in service.
    /// </summary>
    public sealed class EventLogger : LoggerBase
    {
        ///// <summary>
        ///// The log name
        ///// </summary>
        //private const string LogName = "YaMarketServiceLog";


        /// <summary>
        /// Name of the application
        /// </summary>
        private readonly string _applicationName;


        /// <summary>
        /// Initializes a new instance of the <see cref="EventLogger" /> class.
        /// </summary>
        /// <param name="applicationName">Name of the application.</param>
        public EventLogger(string applicationName, string logName)
            : base(applicationName)
        {
            _applicationName = applicationName;

            if (!EventLog.SourceExists(_applicationName))
            {
                EventLog.CreateEventSource(_applicationName, logName);
            }
        }


        public override void DebugMessage(string message)
        {
        }

        /// <summary>
        /// Add information for the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        public override void Information(string message)
        {
            EventLog.WriteEntry(_applicationName, message, EventLogEntryType.Information);
        }

        /// <summary>
        /// Add warning for the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        public override void Warning(string message)
        {
            EventLog.WriteEntry(_applicationName, message, EventLogEntryType.Warning);
        }

        /// <summary>
        /// Add error for the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        public override void Error(string message)
        {
            EventLog.WriteEntry(_applicationName, message, EventLogEntryType.Error);
        }

        /// <summary>
        /// Add errors for the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="exception">The exception.</param>
        public override void Error(string message, Exception exception)
        {
            StringBuilder messageLogBuilder = new StringBuilder();
            messageLogBuilder.AppendFormat("Message: {0}", message);
            messageLogBuilder.AppendLine();
            messageLogBuilder.AppendLine();
            messageLogBuilder.AppendFormat("Error: {0}", exception.Message);
            messageLogBuilder.AppendLine();
            messageLogBuilder.AppendFormat("Stack Trace: {0}", exception.StackTrace);

            Error(messageLogBuilder.ToString());
        }
    }
}