This libraries contains of miscellaneous utilities, which were collected over the years.

## Oxozle Utilities 
[![NuGet Version](http://img.shields.io/nuget/v/Oxozle.Utilities.svg?style=flat)](https://www.nuget.org/packages/Oxozle.Utilities/) [![NuGet Downloads](http://img.shields.io/nuget/dt/Oxozle.Utilities.svg?style=flat)](https://www.nuget.org/packages/Oxozle.Utilities/) 

Currency, date and text formatters. Dictionary Helper. Validation and XML.

##Oxozle Utilities Web 
[![NuGet Version](http://img.shields.io/nuget/v/Oxozle.Utilities.Web.svg?style=flat)](https://www.nuget.org/packages/Oxozle.Utilities.Web/) [![NuGet Downloads](http://img.shields.io/nuget/dt/Oxozle.Utilities.Web.svg?style=flat)](https://www.nuget.org/packages/Oxozle.Utilities.Web/) 

* AntiSpam 
* Server Handlers
* Sitemap
* Cookies, Cache, Requests

##Oxozle Utilities Win 
[![NuGet Version](http://img.shields.io/nuget/v/Oxozle.Utilities.Win.svg?style=flat)](https://www.nuget.org/packages/Oxozle.Utilities.Win/) [![NuGet Downloads](http://img.shields.io/nuget/dt/Oxozle.Utilities.Win.svg?style=flat)](https://www.nuget.org/packages/Oxozle.Utilities.Win/) 

* AsyncPool (clever wrapper for backgroundworker)
* HttpClient, OSHelper


## Copyright

Copyright 2014-2015 Dmitriy Azarov and contributors

## License

Oxozle Utilities is licensed under [MIT](http://www.opensource.org/licenses/mit-license.php "Read more about the MIT license form"). Refer to license.txt for more information.