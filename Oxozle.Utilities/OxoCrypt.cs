﻿// *********************************************************************************
// Author:	Azarov Dmitriy
// Email:	oxozle@gmail.com
// Date: 	30.06.2014
// Project:	Oxozle.Utilities.OxoCrypt.cs
//  
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// *********************************************************************************

#region Usings

using System;
using System.Security.Cryptography;
using System.Text;

#endregion

namespace Oxozle.Utilities
{
    public static class OxoCrypt
    {
        /// <summary>
        /// MD5 hash
        /// </summary>
        public static string MD5(string str)
        {
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            byte[] bs = Encoding.UTF8.GetBytes(str);
            bs = md5.ComputeHash(bs);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bs.Length; i++)
                sb.Append(bs[i].ToString("x2").ToLower());

            return sb.ToString();
        }

        /// <summary>
        /// Encode to base64 (ASCII)
        /// </summary>
        public static string EncodeToBase64(string toEncode)
        {
            byte[] toEncodeAsBytes = Encoding.ASCII.GetBytes(toEncode);
            string returnValue = Convert.ToBase64String(toEncodeAsBytes);

            return returnValue;
        }


        /// <summary>
        /// Encode to base64 UTF
        /// </summary>
        public static string EncodeToBase64UTF(string toEncode)
        {
            byte[] toEncodeAsBytes = Encoding.UTF8.GetBytes(toEncode);
            string returnValue = Convert.ToBase64String(toEncodeAsBytes);

            return returnValue;
        }


        /// <summary>
        /// Decode from base64 (ASCII)
        /// </summary>
        public static string DecodeFromBase64(string encodedData)
        {
            byte[] encodedDataAsBytes = Convert.FromBase64String(encodedData);
            string returnValue = Encoding.ASCII.GetString(encodedDataAsBytes);

            return returnValue;
        }

        /// <summary>
        /// Decode from base64 UTF
        /// </summary>
        public static string DecodeFromBase64UTF(string encodedData)
        {
            byte[] encodedDataAsBytes = Convert.FromBase64String(encodedData);
            string returnValue = Encoding.UTF8.GetString(encodedDataAsBytes);

            return returnValue;
        }
    }
}