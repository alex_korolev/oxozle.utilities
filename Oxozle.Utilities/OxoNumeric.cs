﻿// *********************************************************************************
// Author:	Azarov Dmitriy
// Email:	oxozle@gmail.com
// Date: 	30.06.2014
// Project:	Oxozle.Utilities.OxoNumeric.cs
//  
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// *********************************************************************************

namespace Oxozle.Utilities
{
    public static class OxoNumeric
    {
        private const long Kilobyte = 1024;
        private const long Megabyte = 1024*Kilobyte;
        private const long Gigabyte = 1024*Megabyte;
        private const long Terabyte = 1024*Gigabyte;

        public static string ToFileSizeString(this long bytes)
        {
            if (bytes > Terabyte) return (bytes/Terabyte).ToString("0.00 TB");
            if (bytes > Gigabyte) return (bytes/Gigabyte).ToString("0.00 GB");
            if (bytes > Megabyte) return (bytes/Megabyte).ToString("0.00 MB");
            if (bytes > Kilobyte) return (bytes/Kilobyte).ToString("0.00 KB");
            return bytes + " bytes";
        }
    }
}