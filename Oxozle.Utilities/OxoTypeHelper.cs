﻿// *********************************************************************************
// Author:	Azarov Dmitriy
// Email:	oxozle@gmail.com
// Date: 	30.06.2014
// Project:	Oxozle.Utilities.OxoTypeHelper.cs
//  
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// *********************************************************************************

#region Usings

using System;
using System.Collections.Generic;
using System.Linq;

#endregion

namespace Oxozle.Utilities
{
    public static class OxoTypeHelper
    {
        /// <summary>
        ///   Возвращает список из перечисления
        /// </summary>
        public static List<T> GetList<T>()
        {
            return Enum.GetValues(typeof (T)).Cast<T>().ToList();
        }
    }
}