﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oxozle.Utilities.Attributes
{
    /// <summary>
    ///   Для перевода или присваивания полю русского текста
    /// </summary>
    public class RuText : Attribute
    {
        public string Text;
        public string Genitive;

        public RuText(string text)
        {
            Text = text;
        }

        public RuText(string text, string genitive)
        {
            Text = text;
            Genitive = genitive;
        }
    }
}
