﻿// *********************************************************************************
// Author:	Azarov Dmitriy
// Email:	oxozle@gmail.com
// Date: 	30.06.2014
// Project:	Oxozle.Utilities.OxoValidation.cs
//  
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// *********************************************************************************

#region Usings

using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;

#endregion

namespace Oxozle.Utilities
{
    //[DebuggerStepThrough]
    public static class OxoValidation
    {
        public const string EmailRegexPattern = @"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*";


        public const double Zero = 0.0001;
        private static Regex _int32Regex;
        private static Regex _emailRegex;

        private static readonly CultureInfo EnCulture = new CultureInfo("en-us");
        private static readonly CultureInfo RuCulture = new CultureInfo("ru-ru");


        /// <summary>
        /// Int32 regex
        /// </summary>
        public static Regex Int32Regex
        {
            get
            {
                if (_int32Regex == null)
                    _int32Regex = OxoRegex.CreateRegex(@"^(?:\+|-)?1?\d{1,9}$");

                return _int32Regex;
            }
        }

        /// <summary>
        /// Email regex
        /// </summary>
        public static Regex EmailRegex
        {
            get
            {
                if (_emailRegex == null)
                    _emailRegex = OxoRegex.CreateRegex(EmailRegexPattern);

                return _emailRegex;
            }
        }


        /// <summary>
        /// Return true if value is Email
        /// </summary>
        public static bool IsEmail(this object value)
        {
            return (value != null && value != DBNull.Value && EmailRegex.IsMatch(value.ToString()));
        }


        /// <summary>
        /// Get double Culture invariant
        /// </summary>
        public static double GetDouble(this object value, double defaultValue)
        {
            if (value == null || value == DBNull.Value)
                return defaultValue;


            string str = value.ToString();

            double result;

            //Try parsing in the current culture
            if (!double.TryParse(str, NumberStyles.Any, RuCulture, out result) &&
                //Then try in US english
                !double.TryParse(str, NumberStyles.Any, EnCulture, out result) &&
                //Then in neutral language
                !double.TryParse(str, NumberStyles.Any, CultureInfo.InvariantCulture, out result))
            {
                result = defaultValue;
            }

            return result;
        }


        public static decimal GetDecimal(this object value, decimal defaultValue)
        {
            if (value == null || value == DBNull.Value)
                return defaultValue;


            string str = value.ToString();


            decimal result;

            //Try parsing in the current culture
            if (!decimal.TryParse(str, NumberStyles.Any, RuCulture, out result) &&
                //Then try in US english
                !decimal.TryParse(str, NumberStyles.Any, EnCulture, out result) &&
                //Then in neutral language
                !decimal.TryParse(str, NumberStyles.Any, CultureInfo.InvariantCulture, out result))
            {
                result = defaultValue;
            }

            return result;
        }


        /// <summary>
        /// Return Integer
        /// </summary>        
        public static int GetInteger(this string value, int defaultValue)
        {
            if (value == null)//|| value == DBNull.Value)
                return defaultValue;

            if (!(value is int) && !Int32Regex.IsMatch(value.ToString()))
                return defaultValue;


            //if (value is int)
            //    return (int)value;

            if (value.IsEmpty())
                return defaultValue;

            return Convert.ToInt32(value);
        }

        /// <summary>
        /// Private (VS IS object)
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private static bool IsEmpty(this object value)
        {
            if (value == null || value == DBNull.Value)
                return true;

            return value.ToString().IsEmpty();
        }

        public static bool IsEmpty(this string value)
        {
            return string.IsNullOrEmpty(value);
        }



        /// <summary>
        /// Return Long
        /// </summary>
        public static long GetLong(this object value, long defaultValue)
        {
            if (value == DBNull.Value || value == null)
            {
                return defaultValue;
            }

            if (value is long)
                return (long)value;

            if (value.IsEmpty())
                return defaultValue;

            return Convert.ToInt64(value);
        }


        /// <summary>
        /// Retrun short
        /// </summary>
        public static short GetShort(this object value, short defaultValue)
        {
            if (value == null || value == DBNull.Value)
                return defaultValue;

            //bug short regex
            if (!(value is short) && !Int32Regex.IsMatch(value.ToString()))
                return defaultValue;

            if (value is short)
            {
                return (short)value;
            }

            return Convert.ToInt16(value);
        }


        /// <summary>
        /// Парсит дату
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static DateTime? GetDate(this object value)
        {
            if (value == null)
                return null;

            string strValue = value.ToString();

            if (string.IsNullOrEmpty(strValue))
                return null;

            DateTime date = DateTime.Now;
            bool success = DateTime.TryParse(strValue, out date);

            if (success)
                return date;

            return null;
        }


        /// <summary>
        /// Return true if object value is boolean
        /// </summary>
        public static bool IsBoolean(this object value)
        {
            if (value == null || value == DBNull.Value)
                return false;

            string strValue = value.ToString().ToLowerSafe();


            if (!(value is bool) && strValue.IsEmpty() ||
                ((!(strValue == "0") && !(strValue == "1"))
                 && (!(strValue == "true") && !(strValue == "false"))))
            {
                return false;
            }
            return true;
        }


        /// <summary>
        /// Check file name is valid (and repair if not)
        /// </summary>
        public static string ValidateFileName(this string fileName)
        {
            // Get a list of invalid path characters.
            char[] invalidPathChars = Path.GetInvalidPathChars();

            if (fileName == null)
                return null;

            fileName = fileName.ToLower();

            fileName = fileName
                .Replace(" ", "")
                .Replace(" ", "")
                .Replace(" ", "")
                .Replace(".", "")
                .Replace("*", "")
                .Replace("/", "")
                .Replace("\\", "")
                .Replace("\\", "")
                .Replace("\\", "");

            foreach (char invalidPathChar in invalidPathChars)
            {
                fileName = fileName.Replace(invalidPathChar, '-');
            }
            fileName = fileName.Replace("--", "-").Replace("--", "-").Replace("--", "-");


            fileName = OxoEntryCode.Translite(fileName);
            return fileName;
        }

        /// <summary>
        /// Parse enum string with default value
        /// </summary>
        public static TEnum ToEnum<TEnum>(this string strEnumValue, TEnum defaultValue)
        {
            int value = GetInteger(strEnumValue, 0);
            if (!Enum.IsDefined(typeof(TEnum), value))
                return defaultValue;

            return (TEnum)Enum.Parse(typeof(TEnum), strEnumValue);
        }

        /// <summary>
        /// Parse enum string with default value (short)
        /// </summary>
        public static TEnum ToEnumShort<TEnum>(this string strEnumValue, TEnum defaultValue)
        {
            short value = GetShort(strEnumValue, 0);
            if (!Enum.IsDefined(typeof(TEnum), value))
                return defaultValue;

            return (TEnum)Enum.Parse(typeof(TEnum), strEnumValue);
        }

        /// <summary>
        /// Parse enum string with default value (short)
        /// </summary>
        public static TEnum ToEnumShort<TEnum>(this short strEnumValue, TEnum defaultValue)
        {
            return ToEnumShort(strEnumValue.ToString(), defaultValue);
        }


        /// <summary>
        /// Round to with perido (example multiplies 30)
        /// </summary>
        public static int RoundTo(this int toRound, int period)
        {
            int tmp;
            if ((tmp = (toRound % period)) != 0)
                toRound += (toRound > -1 ? (period - tmp) : -tmp);

            return toRound;
        }

        /// <summary>
        /// Round to with perido (example multiplies 30)
        /// </summary>
        public static short RoundTo(this short toRound, short period)
        {
            //Округляем до 30
            short tmp;
            if ((tmp = (short)(toRound % period)) != 0)
                toRound += (short)(toRound > -1 ? (period - tmp) : -tmp);

            return toRound;
        }


        /// <summary>
        ///   Возвращае bool значение
        /// </summary>
        public static bool GetBoolean(this object value, bool defaultValue)
        {
            if (value == null || value == DBNull.Value)
            {
                return defaultValue;
            }

            if (value is bool)
            {
                return (bool)value;
            }

            if (!(value is string))
            {
                return Convert.ToBoolean(value);
            }
            string strValue = ((string)value).ToLower();
            if (strValue.IsEmpty())
            {
                return defaultValue;
            }

            if (!(strValue == "true") && !(strValue == "1"))
            {
                if ((strValue == "false") || (strValue == "0"))
                {
                    return false;
                }
                return defaultValue;
            }
            return true;
        }



        /// <summary>
        ///   Возвращает строку
        /// </summary>
        public static string GetString(this object value, string defaultValue)
        {
            return GetString(value, defaultValue, null);
        }

        /// <summary>
        ///   Возвращает строку
        /// </summary>
        public static string GetString(this object value, string defaultValue, string culture)
        {
            if (value is string)
            {
                return (string)value;
            }
            if ((value == DBNull.Value) || (value == null))
            {
                return defaultValue;
            }
            if (culture != null)
            {
                CultureInfo cultureInfo = GetCurrentThreadCultureInfo(ref culture);
                return Convert.ToString(value, cultureInfo);
            }
            return Convert.ToString(value);
        }


        /// <summary>
        ///   Возвращает культура потока
        /// </summary>
        private static CultureInfo GetCurrentThreadCultureInfo(ref string culture)
        {
            if (culture == null)
            {
                culture = Thread.CurrentThread.CurrentUICulture.IetfLanguageTag;
                return Thread.CurrentThread.CurrentUICulture;
            }
            return new CultureInfo(culture);
        }

        public static string GetDigis(this string phone)
        {
            return new String(phone.ToCharArray().Where(c => Char.IsDigit(c)).ToArray());
        }
    }
}