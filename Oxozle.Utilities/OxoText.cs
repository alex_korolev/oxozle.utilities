﻿// *********************************************************************************
// Author:	Azarov Dmitriy
// Email:	oxozle@gmail.com
// Date: 	30.06.2014
// Project:	Oxozle.Utilities.OxoText.cs
//  
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// *********************************************************************************

#region Usings

using System;
using System.Text;
using System.Text.RegularExpressions;
using JetBrains.Annotations;

#endregion

namespace Oxozle.Utilities
{
    public static class OxoText
    {


        /// <summary>
        /// Compiled regular expression for performance.
        /// </summary>
        static Regex _htmlRegex = new Regex("<.*?>", RegexOptions.Compiled);

        /// <summary>
        /// Compiled regular expression for performance.
        /// </summary>
        static Regex _htmlRegexWithValues = new Regex("<[^>]*(>|$)", RegexOptions.Compiled);

        /// <summary>
        /// Remove HTML from string with compiled Regex.
        /// </summary>
        public static string RemoveHTML(this string source)
        {
            return _htmlRegex.Replace(source, string.Empty);
        }


        /// <summary>
        /// Remove HTML from string with compiled Regex.
        /// </summary>
        public static string RemoveHTMLWithValues(this string source)
        {
            return _htmlRegexWithValues.Replace(source, string.Empty);
        }


        public static string ToLowerSafe(this string text)
        {
            if (string.IsNullOrEmpty(text))
                return string.Empty;

            return text.ToLower();
        }

        /// <summary>
        /// Форматирование
        /// </summary>
        [StringFormatMethod("input")]
        public static string F(this String input, params object[] args)
        {
            if (input == null)
                return string.Empty;

            return string.Format(input, args);
        }

        /// <summary>
        ///   Обрезает лишнее (не бросает исключение, если объект = null)
        /// </summary>
        public static string Trims(this string str)
        {
            if (string.IsNullOrEmpty(str))
                return null;

            return str.Trim();
        }


        /// <summary>
        ///   Поднимает первую букву все остальные опускает
        /// </summary>
        public static string ToUpperFirstLetter(this string source)
        {
            if (string.IsNullOrEmpty(source))
                return string.Empty;

            source = source.Trim();
            if (string.IsNullOrEmpty(source))
                return string.Empty;

            return char.ToUpper(source[0]) + source.Substring(1).ToLower();
        }

        /// <summary>
        ///   Поднимает все первые буквы в словах
        /// </summary>
        /// <param name="source"> </param>
        /// <returns> </returns>
        public static string ToUpperAllWords(this string source)
        {
            if (string.IsNullOrEmpty(source))
                return null;
            string[] splitted = source.Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);
            if (splitted.Length == 0)
                return string.Empty;

            StringBuilder sb = new StringBuilder();
            foreach (string splitValue in splitted)
            {
                sb.Append(splitValue.ToUpperFirstLetter());
                sb.Append(" ");
            }

            string result = sb.ToString();

            if (result.Length > 2)
                result = result.Substring(0, result.Length - 1);

            return result;
        }


        /// <summary>
        ///   Возвращает null если строка пустая
        /// </summary>
        public static string NullIfEmpty(this string str)
        {
            if (string.IsNullOrEmpty(str))
                return null;

            return str;
        }

        /// <summary>
        /// Заменяет двойные пробелы на одинарные
        /// http://stackoverflow.com/questions/206717/how-do-i-replace-multiple-spaces-with-a-single-space-in-c
        /// </summary>
        public static string RemoveMultipleSpaces(this string text)
        {
            if (string.IsNullOrEmpty(text))
                return string.Empty;

            const RegexOptions options = RegexOptions.None;
            Regex regex = new Regex(@"[ ]{2,}", options);
            return regex.Replace(text, @" ");
        }


        /// <summary>
        ///   Возвращает окончание исчисляемых
        /// </summary>
        /// <param name="count"> Кол-во </param>
        /// <param name="nominativeEnd"> Товар </param>
        /// <param name="genitiveEndOne"> Товара </param>
        /// <param name="genitiveEndMany"> Товаров </param>
        public static string GetCountEnd(this int count, string nominativeEnd, string genitiveEndOne,
            string genitiveEndMany)
        {
            if (count < 0)
                throw new ArgumentException("count");


            int lastDigit = count % 10;
            int lastTwoDigits = count % 100;
            if (lastDigit == 1 && lastTwoDigits != 11)
            {
                return nominativeEnd;
            }
            if (lastDigit == 2 && lastTwoDigits != 12 || lastDigit == 3 && lastTwoDigits != 13 ||
                lastDigit == 4 && lastTwoDigits != 14)
            {
                return genitiveEndOne;
            }
            return genitiveEndMany;
        }

        /// <summary>
        ///   Ограничить длину текста, добавив в конце "..."
        /// </summary>
        public static string LimitLength(this string text, int maxLength)
        {
            return LimitLength(text, maxLength, "...");
        }

        /// <summary>
        ///   Ограничить длину текста.
        /// </summary>
        public static string LimitLength(this string text, int maxLength, string padString)
        {
            if (text == null)
            {
                return null;
            }
            if (maxLength > 0)
            {
                int length = maxLength - padString.Length;
                if (text.Length > length)
                {
                    text = text.Substring(0, length);
                    text = text + padString;
                }
            }
            return text;
        }

        /// <summary>
        ///   Подготавливает текст к возможности записи в тег alt
        /// </summary>
        /// <param name="text"> </param>
        /// <returns> </returns>
        public static string ToAltText(this string text)
        {
            if (string.IsNullOrEmpty(text))
                return string.Empty;

            text = text.Replace("\"", string.Empty).Replace("'", string.Empty);
            return text;
        }
    }
}