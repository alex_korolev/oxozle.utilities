﻿// *********************************************************************************
// Author:	Azarov Dmitriy
// Email:	oxozle@gmail.com
// Date: 	30.06.2014
// Project:	Oxozle.Utilities.OxoXMLAccess.cs
//  
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// *********************************************************************************

#region Usings

using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

#endregion

namespace Oxozle.Utilities
{
    /// <summary>
    ///   Класс сохранения и загрузки сериализуемых данных в файлы
    /// </summary>
    /// <typeparam name = "KeyType">Тип сериализуемых данных</typeparam>
    public static class OxoXMLAccess
    {
        public static string SerializeToMemory<KeyType>(object obj)
        {
            try
            {
                Type typeof_object = typeof (KeyType);
                XmlSerializer xmlser = new XmlSerializer(typeof_object);
                StringWriter sw = new StringWriter();
                xmlser.Serialize(sw, obj);
                return (sw.ToString());
            }
            catch (Exception exception)
            {
                //TrailLogger.Logger.Error(exception);
            }

            return null;
        }

        /// <summary>
        ///   Из строки в объект
        /// </summary>
        /// <typeparam name="KeyType"> </typeparam>
        /// <param name="Str"> Зашифрованная строка </param>
        /// <returns> </returns>
        public static KeyType DeserializeFromMemory<KeyType>(string Str)
        {
            object result = null;
            try
            {
                Type type = typeof (KeyType);
                XmlSerializer serializer = new XmlSerializer(type);


                using (TextReader reader = new StringReader(Str))
                {
                    result = serializer.Deserialize(reader);
                }

                return (KeyType) result;
            }
            catch (Exception exception)
            {
                //TrailLogger.Logger.Error(exception);
            }

            return (KeyType) result;
        }


        /// <summary>
        ///   Загрудить объект данных из файла
        /// </summary>
        /// <param name = "filePath">Путь к файлу xml</param>
        /// <returns>Объект KeyType загруженный из файла</returns>
        public static KeyType LoadFile<KeyType>(string filePath)
        {
            return (KeyType) XMLDeserializeObject(filePath, typeof (KeyType));
        }


        /// <summary>
        ///   Сохранить данные в файл
        /// </summary>
        /// <param name = "filePath">Путь</param>
        /// <param name = "value">Объект класса KeyType</param>
        public static void SaveFile<KeyType>(string filePath, KeyType value)
        {
            XMLSerializeObject(filePath, value, typeof (KeyType));
        }

        #region Serialization

        /// <summary>
        ///   Серилизация объекта
        /// </summary>
        private static void XMLSerializeObject(string FileName, object obj, Type typeof_object)
        {
            Stream fs = null;
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof_object);
                fs = new FileStream(FileName, FileMode.Create, FileAccess.ReadWrite);
                XmlWriter writer = new XmlTextWriter(fs, Encoding.Unicode);
                serializer.Serialize(writer, obj);
                writer.Close();
                fs.Close();
            }
            catch (Exception e)
            {
                if (fs != null)
                    fs.Close();
            }
        }

        /// <summary>
        ///   Десерилизация объекта
        /// </summary>
        private static object XMLDeserializeObject(string FileName, Type typeof_object)
        {
            FileStream fs = null;
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof_object);
                fs = new FileStream(FileName, FileMode.Open);
                XmlReader reader = new XmlTextReader(fs);
                object obj;
                obj = serializer.Deserialize(reader);
                reader.Close();
                return obj;
            }
            catch (Exception exception)
            {
                if (fs != null)
                    fs.Close();
            }

            return null;
        }

        #endregion
    }
}