﻿// *********************************************************************************
// Author:	Azarov Dmitriy
// Email:	oxozle@gmail.com
// Date: 	30.06.2014
// Project:	Oxozle.Utilities.OxoEntryCode.cs
//  
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// *********************************************************************************

#region Usings

using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

#endregion

namespace Oxozle.Utilities
{
    /// <summary>
    ///   Класс для управления кодами сущностей
    /// </summary>
    public static class OxoEntryCode
    {
        /// <summary>
        ///   Генерирует код сущности по имени (в транслите). Допускаются только буквы цифры и знак пробела. Все остальные символы будут удалены. Обрезаются лишние пробелы
        /// </summary>
        /// <returns> </returns>
        public static string GenerateCode(string input, bool toLower = true)
        {
            string words = input ?? "";

            words = words.Trim();
            words = Regex.Replace(words, "  ", " ");


            string result = "";
            foreach (char c in words)
            {
                if (Char.IsLetter(c) || c == ' ' || Char.IsDigit(c) || c == '-')
                {
                    result += c;
                }
            }

            result = Regex.Replace(result, " ", "-");
            result = Regex.Replace(result, "--", "-");

            result = Translite(result);

            if (toLower)
            {
                result = result.ToLower();
            }
            return result;
        }


        public static string Translite(string stringToTranslite)
        {
            Dictionary<string, string> words = new Dictionary<string, string>();
            words.Add("а", "a");
            words.Add("б", "b");
            words.Add("в", "v");
            words.Add("г", "g");
            words.Add("д", "d");
            words.Add("е", "e");
            words.Add("ё", "yo");
            words.Add("ж", "zh");
            words.Add("з", "z");
            words.Add("и", "i");
            words.Add("й", "j");
            words.Add("к", "k");
            words.Add("л", "l");
            words.Add("м", "m");
            words.Add("н", "n");
            words.Add("о", "o");
            words.Add("п", "p");
            words.Add("р", "r");
            words.Add("с", "s");
            words.Add("т", "t");
            words.Add("у", "u");
            words.Add("ф", "f");
            words.Add("х", "h");
            words.Add("ц", "c");
            words.Add("ч", "ch");
            words.Add("ш", "sh");
            words.Add("щ", "sch");
            words.Add("ъ", "j");
            words.Add("ы", "i");
            words.Add("ь", "j");
            words.Add("э", "e");
            words.Add("ю", "yu");
            words.Add("я", "ya");
            words.Add("А", "A");
            words.Add("Б", "B");
            words.Add("В", "V");
            words.Add("Г", "G");
            words.Add("Д", "D");
            words.Add("Е", "E");
            words.Add("Ё", "Yo");
            words.Add("Ж", "Zh");
            words.Add("З", "Z");
            words.Add("И", "I");
            words.Add("Й", "J");
            words.Add("К", "K");
            words.Add("Л", "L");
            words.Add("М", "M");
            words.Add("Н", "N");
            words.Add("О", "O");
            words.Add("П", "P");
            words.Add("Р", "R");
            words.Add("С", "S");
            words.Add("Т", "T");
            words.Add("У", "U");
            words.Add("Ф", "F");
            words.Add("Х", "H");
            words.Add("Ц", "C");
            words.Add("Ч", "Ch");
            words.Add("Ш", "Sh");
            words.Add("Щ", "Sch");
            words.Add("Ъ", "J");
            words.Add("Ы", "I");
            words.Add("Ь", "J");
            words.Add("Э", "E");
            words.Add("Ю", "Yu");
            words.Add("Я", "Ya");

            foreach (KeyValuePair<string, string> pair in words)
            {
                stringToTranslite = stringToTranslite.Replace(pair.Key, pair.Value);
            }
            return stringToTranslite;
        }

        public static string GenerateFileName(string input)
        {
            string words = input ?? string.Empty;

            words = words.Trim();
            words = Regex.Replace(words, "  ", " ");
            words = Regex.Replace(words, " ", "-");


            string result = "";
            foreach (char c in words)
            {
                if (Char.IsLetter(c) || Char.IsDigit(c) || c == '-')
                {
                    result += c;
                }
            }

            result = Translite(result).ToLower();
            return result;
        }
    }
}