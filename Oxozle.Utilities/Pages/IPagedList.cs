﻿namespace Oxozle.Utilities.Pages
{
    public interface IPagedList
    {
        int TotalPages { get; set; }
        int TotalCount { get; set; }
        int CurrentIndex { get; set; }
        int PageSize { get; set; }
        int CurrentPage { get; }
        bool IsPreviousPage { get; }
        bool IsNextPage { get; }
    }
}
