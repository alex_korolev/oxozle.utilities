﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Configuration;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Diagnostics;
using System.IO;

namespace Oxozle.Utilities.Mail
{
    public sealed class MailerPro
    {

        /// <summary>
        /// Если установлено, то вся почта идет туда
        /// </summary>
        public static string DebugOnlyTo = null;

        static MailerPro()
        {
        }


        public MailAddress FromAddress { get; private set; }

        private readonly SmtpClient _smtp;

        public MailerPro(string sectionName = "default")
        {
            SmtpSection smtp = (SmtpSection)ConfigurationManager.GetSection("mailSettings/" + sectionName);

            FromAddress = new MailAddress(smtp.From);

            _smtp = new SmtpClient
            {
                Host = smtp.Network.Host,
                Port = smtp.Network.Port,
                EnableSsl = smtp.Network.EnableSsl,
                DeliveryMethod = smtp.DeliveryMethod,
                UseDefaultCredentials = smtp.Network.DefaultCredentials,
                Credentials = new NetworkCredential(smtp.Network.UserName, smtp.Network.Password)
            };
        }

        public void Send(string toEmail, string subject, string body, string copy = null, string hiddenCopy = null, bool isBodyHtml = true, string[] attachments = null, Stream attachmentStream = null, string attachmentStreamFileName = null)
        {

            if (!DebugOnlyTo.IsEmpty())
            {
                subject = "[ТЕСТОВОЕ СООБЩЕНИЕ] " + subject;
                toEmail = DebugOnlyTo;
                copy = null;
                hiddenCopy = null;
            }

            if (string.IsNullOrEmpty(toEmail))
            {
                return;
            }

            string[] addresses = toEmail.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries);

            foreach (MailAddress toAddress in addresses.Select(address => new MailAddress(address)))
            {
                SendEmail(toAddress, subject, body, copy, hiddenCopy, isBodyHtml, attachments, attachmentStream, attachmentStreamFileName);
            }
        }

        private void SendEmail(MailAddress to, string subject, string body, string copy, string hiddenCopy, bool isHtml, string[] attachments, Stream attachmentStream = null, string attachmentStreamFileName = null)
        {
#if DEBUG && false
            Debug.WriteLine("Send email to " + to.Address);
            return;
#endif

            using (MailMessage message = new MailMessage(FromAddress, to)
            {
                Subject = subject,
                Body = body,
                IsBodyHtml = isHtml,
            })
            {
                if (!copy.IsEmpty())
                {
                    foreach (string cc in copy.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
                    {
                        if (cc.IsEmpty())
                            continue;
                        message.CC.Add(cc);
                    }
                }

                if (!hiddenCopy.IsEmpty())
                {
                    foreach (string bcc in hiddenCopy.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
                    {
                        if (bcc.IsEmpty())
                            continue;
                        message.Bcc.Add(bcc);
                    }
                }


                if (attachments != null && attachments.Length > 0)
                    foreach (string attachment in attachments)
                        message.Attachments.Add(new Attachment(attachment));

                if (attachmentStream != null && attachmentStream.Length > 0)
                    message.Attachments.Add(new Attachment(attachmentStream, attachmentStreamFileName));

                _smtp.Send(message);
            }
        }
    }
}