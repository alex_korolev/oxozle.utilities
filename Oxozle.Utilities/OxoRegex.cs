﻿// *********************************************************************************
// Author:	Azarov Dmitriy
// Email:	oxozle@gmail.com
// Date: 	30.06.2014
// Project:	Oxozle.Utilities.OxoRegex.cs
//  
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// *********************************************************************************

#region Usings

using System.Text.RegularExpressions;

#endregion

namespace Oxozle.Utilities
{
    public static class OxoRegex
    {
        private static RegexOptions _defaultOptions = RegexOptions.Compiled;

        /// <summary>
        ///   Настройки по умолчанию
        /// </summary>
        public static RegexOptions DefaultOptions
        {
            get { return _defaultOptions; }
            set { _defaultOptions = value; }
        }

        /// <summary>
        ///   Возвращает Regex для шаблона
        /// </summary>
        public static Regex CreateRegex(string pattern)
        {
            return CreateRegex(pattern, DefaultOptions);
        }

        /// <summary>
        ///   Возвращает Regex для шаблона
        /// </summary>
        public static Regex CreateRegex(string pattern, RegexOptions options)
        {
            return new Regex(pattern, options);
        }
    }
}