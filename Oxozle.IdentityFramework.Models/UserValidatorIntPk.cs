﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace Oxozle.IdentityFramework.Models
{
    /// <summary>
    /// https://gist.github.com/blachniet/7394005
    /// </summary>
    public class UserValidatorIntPk : UserValidator<ApplicationUser, int>
    {
        private UserManager<ApplicationUser, int> _manager;


        public UserValidatorIntPk(UserManager<ApplicationUser, int> manager)
            : base(manager)
        {
            _manager = manager;
        }

        public async override Task<IdentityResult> ValidateAsync(ApplicationUser item)
        {
            var errors = new List<string>();
            if (!IsValid(item.Email))
                errors.Add("Укажите верный email");

            if (_manager != null)
            {
                var otherAccount = await _manager.FindByEmailAsync(item.Email);
                if (otherAccount != null && otherAccount.Id != item.Id)
                    errors.Add("Выберите другой email. Аккаунт с таким email уже существует.");
            }

            return errors.Any()
                ? IdentityResult.Failed(errors.ToArray())
                : IdentityResult.Success;
        }

        public bool IsValid(string emailaddress)
        {
            try
            {
                MailAddress m = new MailAddress(emailaddress);

                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }
    }
}
