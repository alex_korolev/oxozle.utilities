﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;

namespace Oxozle.IdentityFramework.Models
{
    public class ApplicationUserManager : UserManager<ApplicationUser, int>
    {
        public ApplicationUserManager(IUserStore<ApplicationUser, int> store)
            : base(store)
        {
        }

        //public static UserStoreIntPk APPCreateUserStore(IOwinContext context)
        //{
        //    return new UserStoreIntPk(context.Get<ApplicationDbContext>());
        //}


        public static ApplicationUserManager CreateStatic(ApplicationDbContext context)
        {
            ApplicationUserManager manager = new ApplicationUserManager(new UserStoreIntPk(context));
            manager.UserValidator = new UserValidatorIntPk(manager);

            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 1,
            };

            return manager;
        }



        public static ApplicationUserManager Create(IdentityFactoryOptions<ApplicationUserManager> options,
            IOwinContext context)
        {
            ApplicationUserManager manager = new ApplicationUserManager(new UserStoreIntPk(context.Get<ApplicationDbContext>()));
            manager.UserValidator = new UserValidatorIntPk(manager);

            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 1,
            };

            return manager;
        }
    }
}
